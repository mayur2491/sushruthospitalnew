<!DOCTYPE html>
<html lang="en">
  <head>
        @include('layout.head') 
        <link href="public/vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        @include('layout.left-sidebar')
        @include('layout.topnav')
        <div class="right_col" role="main">
                <div class="row">
                      <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                          <div class="x_title">
                            <h2>InDoor Patients Catlogue </h2>
                            <div class="clearfix"></div>
                          </div>
                          <div class="x_content" style="display: block;">
                            <br>
                             <div class="panel-body">
                                @if(isset($ipdpatients) && count($ipdpatients)>0)
                                  <table id="patientsdata" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%" >
                                      <thead>
                                      <tr>
                                          <td>Name</td>
                                          <td>Address</td>
                                          <td>Contact</td>
                                          <td>Gender</td>
                                          <td>Date of birth</td>
                                          <td>Action</td>
                                          <td><input type="checkbox" onclick="$('input[name*=\'selected\']').prop('checked', this.checked);"></td>
                                      </tr>
                                      </thead>
                                      <tbody>
                                      @foreach($ipdpatients as $ipdpatient)
                                      <tr>

                                          <td>{{$ipdpatient->name}}</td>
                                          <td>{{$ipdpatient->address}}</td>
                                          <td>{{$ipdpatient->contact}}</td>
                                          <td>{{$ipdpatient->gender}}</td>
                                          <td>{{$ipdpatient->date_of_birth}}</td>
                                          

                                          <td><a class="btn btn-primary edituser"  href="{{$root}}/ipdpetientedit?regno={{$ipdpatient->regno}}"><i class="glyphicon glyphicon-pencil"></i></a></td>
                                          <td><input type="checkbox" name="selected" value="{{$ipdpatient->id}}" class="deleteval"></td>
                                      </tr>

                                      @endforeach
                                      </tbody>
                                  </table>

                            @else
                            No users found...
                            @endif
                            {!! $ipdpatients->render() !!}

                            </div>
                             
                          </div>
                        </div>
                      </div>
               </div>
        </div>
        @include('layout.footer')

        <!-- Modal -->
  </div>
</div>
@include('layout.scripts')
<script> var root="{{$root}}/";</script>
<script src="public/vendors/bootstrap-daterangepicker/daterangepicker.js"></script>
<script src="public/custom-js/patient.js"></script>
   </body>
</html>
