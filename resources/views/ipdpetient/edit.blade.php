<!DOCTYPE html>
<html lang="en">
  <head>
        @include('layout.head') 
        @include('layout.datatables_css')
        
        <link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.css" rel="stylesheet">
        <link href="public/css/bootstrap-datetimepicker.min.css" rel="stylesheet" media="screen">
        <link href="public/vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
        <link href="public/css/custom.css" rel="stylesheet">
        
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        @include('layout.left-sidebar')
        @include('layout.topnav')
        <div class="right_col" role="main">
                <div class="row">
                      <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                          <div class="x_title">
                            <h2>InDoor Patient details</h2>
                            <div class="clearfix"></div>
                          </div>

                          <div class="x_content" style="display: block;">
                            <br>
                            <form id="editipdform" data-parsley-validate="" class="" novalidate="">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}"/>

                              <div class="accordion" id="accordion" role="tablist" aria-multiselectable="true">
                                <div class="panel">
                                  <a class="panel-heading" role="tab" id="headingOne" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                    <h4 class="panel-title">Basic Information</h4>
                                  </a>
                                  <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                                    <div class="panel-body">
                                
                                  <div class="col-md-6 col-sm-12 col-xs-12 form-group">
                                    <label for="name" class="control-label col-md-12 col-sm-12 col-xs-12">Name</label>
                                    <div class="col-md-12 col-sm-6 col-xs-12">
                                      
                                      <input id="name" class="form-control col-md-7 col-xs-12" type="text"  value="{{$patient->name}}" disabled="disabled">
                                    </div>
                                  </div>
                                  
                                  
                                  <div class="col-md-6 col-sm-12 col-xs-12 form-group">
                                    <label for="age" class="control-label col-md-12 col-sm-12 col-xs-12">Age</label>
                                    <div class="col-md-12 col-sm-6 col-xs-12">
                                      
                                      <input id="age" class="form-control col-md-7 col-xs-12" type="text"  value="{{$patient->age}}" disabled="disabled">
                                    </div>
                                  </div>
                                  <div class="col-md-6 col-sm-12 col-xs-12 form-group">
                                    <label for="address" class="control-label col-md-12 col-sm-12 col-xs-12">Address</label>
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                      
                                      <input id="address" class="form-control col-md-7 col-xs-12" type="text"  value="{{$patient->address}}" disabled="disabled">
                                    </div>
                                  </div>

                                  <div class="col-md-6 col-sm-12 col-xs-12 form-group">
                                    <label for="address" class="control-label col-md-12 col-sm-12 col-xs-12">Gender</label>
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                      
                                      <input id="address" class="form-control col-md-7 col-xs-12" type="text"   value="{{$patient->gender}}" disabled="disabled">
                                    </div>
                                  </div>
                                  <div class="col-md-6 col-sm-12 col-xs-12 form-group">
                                    <label for="address" class="control-label col-md-12 col-sm-12 col-xs-12">Ref Doctor</label>
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                      
                                      <input id="address" class="form-control col-md-7 col-xs-12" type="text"  value="{{$patient->refby}}" disabled="disabled">
                                    </div>
                                  </div>

                                  <div class="col-md-6 col-sm-12 col-xs-12 form-group">
                                    <label class="control-label col-md-12 col-sm-12 col-xs-12" for="first-name">RegNo<span class="required">*</span>
                                    </label>
                                    <div class="col-md-12 col-sm-12 col-xs-12" id="regi_type_div">
                                        <input id="regno" class="form-control col-md-7 col-xs-12" type="text" name="data[regno]" value="{{$ipdpatient->regno}}" disabled="disabled">
                                    </div>
                                  </div>

                                  <div class="col-md-6 col-sm-12 col-xs-12 form-group">
                                    <label for="occupation" class="control-label col-md-12 col-sm-12 col-xs-12">Occupation</label>
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                      <input id="occupation"  value="{{$ipdpatient->occupation}}" class="form-control col-md-7 col-xs-12" type="text" name="data[occupation]">
                                    </div>
                                  </div>
                                  <div class="col-md-6 col-sm-12 col-xs-12 form-group">
                                    <label for="address" class="control-label col-md-12 col-sm-12 col-xs-12">Next Of Kins Name</label>
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                      <input id="next_kins_name"  value="{{$ipdpatient->next_kins_name}}" class="form-control col-md-7 col-xs-12" type="text" name="data[next_kins_name]">
                                    </div>
                                  </div>
                                  <div class="col-md-6 col-sm-12 col-xs-12 form-group">
                                    <label for="address" class="control-label col-md-12 col-sm-12 col-xs-12">Kins Adderess</label>
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                      <input id="kins_address"  value="{{$ipdpatient->kins_address}}" class="form-control col-md-7 col-xs-12" type="text" name="data[kins_address]">
                                    </div>
                                  </div>
                                  <div class="col-md-6 col-sm-12 col-xs-12 form-group">
                                    <label for="address" class="control-label col-md-12 col-sm-12 col-xs-12">Kins Contact </label>
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                      <input id="kins_contact"  value="{{$ipdpatient->kins_contact}}" class="form-control col-md-7 col-xs-12" type="text" name="data[kins_contact]">
                                    </div>
                                  </div>

                                
                                <div class="col-md-6 col-sm-12 col-xs-12 form-group">
                                  <label for="address" class="control-label col-md-12 col-sm-12 col-xs-12">Admited On</label>
                                  <div class="input-append date form_datetime col-md-12 col-sm-12 col-xs-12" data-date="2012-12-21T15:25:00Z">
                                      <input class="form-control col-md-7 col-xs-12" id="admited_datetime" size="16" type="text" name="data[admited_datetime]" value="{{$ipdpatient->admited_datetime}}" readonly>
                                     
                                      <span class="add-on"><i class="icon-remove"></i></span>
                                      <span class="add-on"><i class="icon-th"></i></span>
                                  </div>
                                </div>

                                 

                                <div class="col-md-6 col-sm-12 col-xs-12 form-group">
                                  <label for="address" class="control-label col-md-12 col-sm-12 col-xs-12">Discharge On</label>
                                  <div class="input-append date form_datetime col-md-12 col-sm-12 col-xs-12" data-date="2012-12-21T15:25:00Z">
                                      <input class="form-control col-md-7 col-xs-12" id="discharge_datetime" size="16" type="text" name="data[discharge_datetime]" value="{{$ipdpatient->discharge_datetime}}" readonly>
                                      
                                      <span class="add-on"><i class="icon-remove"></i></span>
                                      <span class="add-on"><i class="icon-th"></i></span>
                                  </div>
                                </div>
                                 
                                 <div class="col-md-6 col-sm-12 col-xs-12 form-group">
                                    <label for="operative_and_dignostic_procedure" class="control-label col-md-12 col-sm-12 col-xs-12">Ward No.</label>
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                      <input id="ward_no" class="form-control col-md-7 col-xs-12" type="text" placeholder="Ward No" value="{{$ipdpatient->ward_no}}" name="data[ward_no]">
                                    </div>
                                  </div>

                                   <div class="col-md-6 col-sm-12 col-xs-12 form-group" >
                                     <label class="control-label col-md-12 col-sm-12 col-xs-12" for="first-name">Category<span class="required">*</span>
                                    </label>
                                    <div class="col-md-12 col-sm-12 col-xs-12" id="category_div">
                                      <select class="form-control" name="data[is_mlc]" id="category" onchange="getMlcCount()">
                                        <option value="{{$ipdpatient->is_mlc}}" hidden>{{($ipdpatient->is_mlc==1) ? "MLC" : "Non MLC" }}</option>
                                        <option value="1">MLC</option>
                                        <option value="0">Non MLC</option>
                                      </select>
                                      
                                    </div>
                                    <div class="col-md-4" id="mlccount" style="display:none;"></div>
                                  </div>

                                  <div class="col-md-6 col-sm-12 col-xs-12 form-group">
                                    <label for="address" class="control-label col-md-12 col-sm-12 col-xs-12">HPR Report</label>
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                      <textarea class="resizable_textarea form-control" id="hr_report" name="data[hr_report]"  value="{{$ipdpatient->hr_report}}" placeholder="HR Report">{{$ipdpatient->hr_report}}</textarea>
                                    </div>
                                  </div>

                                  <div class="col-md-6 col-sm-12 col-xs-12 form-group">
                                    <label for="operative_and_dignostic_procedure" class="control-label col-md-12 col-sm-12 col-xs-12">Operative and Dignostic Procedure Carried Out</label>
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                    <textarea class="resizable_textarea form-control" id="operative_and_dignostic_procedure" name="data[operative_and_dignostic_procedure]"  value="{{$ipdpatient->operative_and_dignostic_procedure}}"  placeholder="Operative and dignostic procedure">{{$ipdpatient->operative_and_dignostic_procedure}}</textarea>
                                    </div>
                                  </div>

                                </div>
                              </div>
                            </div>
                          </div>


                                <div class="accordion" id="accordion" role="tablist" aria-multiselectable="true">
                                  <div class="panel">
                                    <a class="panel-heading" role="tab" id="headingtwo" data-toggle="collapse" data-parent="#accordion" href="#collapsetwo" aria-expanded="false" aria-controls="collapsetwo">
                                      <h4 class="panel-title">Treatment</h4>
                                    </a>
                                    <div id="collapsetwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingtwo">
                                      <div class="panel-body">

                                  <div class="col-md-6 col-sm-12 col-xs-12 form-group">
                                    <label for="address" class="control-label col-md-12 col-sm-12 col-xs-12">Name of Surgery </label>
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                      <input id="sergery_name" class="form-control col-md-7 col-xs-12" type="text" name="treatment[surgery_name]" value="{{($treatment)? $treatment->surgery_name :''}}">
                                    </div>
                                  </div>

                                  <div class="col-md-6 col-sm-12 col-xs-12 form-group">
                                    <label for="address" class="control-label col-md-12 col-sm-12 col-xs-12">Indications</label>
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                      <input id="indications" class="form-control col-md-7 col-xs-12" type="text" name="treatment[indications]" value="{{($treatment)? $treatment->indications :''}}">
                                    </div>
                                  </div>


                                  <div class="col-md-6 col-sm-12 col-xs-12 form-group">
                                    <label for="address" class="control-label col-md-12 col-sm-12 col-xs-12">Name Of Surgen</label>
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                      <input id="" class="form-control col-md-7 col-xs-12" type="text" name="treatment[surgen_name]" value="{{($treatment) ? $treatment->surgen_name :''}}">
                                    </div>
                                  </div>

                                  <div class="col-md-6 col-sm-12 col-xs-12 form-group">
                                    <label for="asstt_symptoms" class="control-label col-md-12 col-sm-12 col-xs-12">Assistant Doctor</label>
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                      <input id="" class="form-control col-md-7 col-xs-12" type="text" name="treatment[assistant_doctor]" value="{{($treatment)? $treatment->assistant_doctor :''}}">
                                    </div>
                                  </div>

                                  <div class="col-md-6 col-sm-12 col-xs-12 form-group">
                                    <label for="asstt" class="control-label col-md-12 col-sm-12 col-xs-12">Asstt</label>
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                      <input id="" class="form-control col-md-7 col-xs-12" type="text" name="treatment[asstt]" value="{{($treatment)?  $treatment->asstt :''}}">
                                    </div>
                                  </div>

                                  <div class="col-md-6 col-sm-12 col-xs-12 form-group">
                                    <label for="arasethesist" class="control-label col-md-12 col-sm-12 col-xs-12">Anansthesist</label>
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                      <input id="" class="form-control col-md-7 col-xs-12" type="text" name="treatment[anansthesist]" value="{{($treatment)? $treatment->anansthesist :''}}">
                                    </div>
                                  </div>

                                  <div class="col-md-6 col-sm-12 col-xs-12 form-group">
                                    <label for="analisia" class="control-label col-md-12 col-sm-12 col-xs-12">Type of Analisia</label>
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                     <select class="form-control" name="treatment[analisia]" id="category">

                                        
                                        <option value="{{($treatment)? $treatment->analisia :''}}" hidden>{{($treatment)? $treatment->analisia :''}}</option>
                                        <option value="ga">GA</option>
                                        <option value="sa">SA</option>
                                        <option value="regional">Regional</option>
                                     </select>
                                    </div>
                                  </div>

                                  
                                  <div class="col-md-6 col-sm-12 col-xs-12 form-group">
                                    <label for="Operation Time" class="control-label col-md-12 col-sm-12 col-xs-12">Operation Time</label>
                                    <div class="input-append date form_datetime col-md-12 col-sm-12 col-xs-12" data-date="2012-12-21T15:25:00Z">
                                        <input class="form-control col-md-7 col-xs-12" id="operation_time" size="16" type="text" name="treatment[operation_time]" value="{{ ($treatment)? $treatment->operation_time:''}}" readonly>
                                       
                                        <span class="add-on"><i class="icon-remove"></i></span>
                                        <span class="add-on"><i class="icon-th"></i></span>
                                    </div>
                                  </div>

                                  
                                  <div class="col-md-6 col-sm-12 col-xs-12 form-group">
                                    <label for="Tourniquet Time" class="control-label col-md-12 col-sm-12 col-xs-12">Tourniquet Time</label>
                                    <div class="input-append date form_datetime col-md-12 col-sm-12 col-xs-12" data-date="2012-12-21T15:25:00Z">
                                        <input class="form-control col-md-7 col-xs-12" id="tourniquet_time" size="16" type="text" name="treatment[tourniquet_time]" value="{{($treatment)? $treatment->tourniquet_time :''}}" readonly>
                                       
                                        <span class="add-on"><i class="icon-remove"></i></span>
                                        <span class="add-on"><i class="icon-th"></i></span>
                                    </div>
                                  </div>  

                                  <div class="col-md-6 col-sm-12 col-xs-12 form-group">
                                    <label for="toriqual_time" class="control-label col-md-12 col-sm-12 col-xs-12">Blood Loss</label>
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                     <input id="blood_loss" class="form-control col-md-7 col-xs-12" type="text" name="treatment[blood_loss]" value="{{($treatment)? $treatment->blood_loss :''}}">
                                    </div>
                                  </div>  

                                  <div class="col-md-6 col-sm-12 col-xs-12 form-group">
                                    <label for="toriqual_time" class="control-label col-md-12 col-sm-12 col-xs-12">Operation Findings</label>
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                      <textarea rows="5" class="resizable_textarea form-control" id="hr_report" name="treatment[operation_findings]"  value="{{($treatment)? $treatment->operation_findings:''}}" placeholder="Operation Findings">{{($treatment)?$treatment->operation_findings :''}}</textarea>
                                     
                                    </div>
                                  </div>  

                                  <div class="col-md-6 col-sm-12 col-xs-12 form-group">
                                    <label for="implants_used" class="control-label col-md-12 col-sm-12 col-xs-12">Implants used</label>
                                    <div class="col-md-12 col-sm-12 col-xs-12">

                                      <input id="tags_1" type="text" name="treatment[implants_used]" class="tags form-control" value="{{($treatment)?  $treatment->implants_used:'' }}"/>
                                      <div id="suggestions-container" style="position: relative; float: left; width: 250px; margin: 10px;"></div>
                                     
                                    </div>
                                  </div> 

                                  <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                    <label for="implants_used" class="control-label col-md-12 col-sm-12 col-xs-12">Follow Up Advice</label>
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                          <!-- <div id="summernote"></div> -->
                                          <textarea class="input-block-level" id="summernote" name="treatment[advice]" value="{{($treatment)?  $treatment->advice :''}}">{{($treatment)? $treatment->advice:''}}</textarea>
                                    </div> 
                                  </div>

                                  <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                    <label for="implants_used" class="control-label col-md-12 col-sm-12 col-xs-12">Prescriptions</label>
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                          <!-- <div id="summernote1"></div> -->
                                          <textarea class="input-block-level" id="summernote1" name="treatment[prescriptions]" value="{{ ($treatment)? $treatment->prescriptions :''}}">{{ ($treatment)? $treatment->prescriptions:''}}</textarea>
                                    </div> 
                                  </div>

                                </div>
                              </div>
                            </div>
                          </div>

                                  
                                  <div class="col-md-12 col-sm-12 col-xs-12 mt-top">
                                    <!-- <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3"> -->
                                     <button class="btn btn-primary pull-right" type="submit" id="update-ipdpatient">Update</button>
                                    
                                  <!-- </div> -->
                                  </div>

                            </form>
                            <div class="ln_solid"></div>
                          </div>

                         <!-- Nurse Review section -->
                          <div class="x_title">
                                <h2>Nurse Review</h2>
                                <ul class="nav navbar-right panel_toolbox">
                                   <li><button type="button" class="btn btn-primary" id="addreview" data-toggle="modal" data-target="#nursemodal">Add</button></li>
                                </ul>   
                                <div class="clearfix"></div>
                          </div>
                              
                          <div class="x_content" id="testinfo" style="display: block;">

                              <table id="datatable" class="table table-bordered table-striped dt-responsive nowrap" cellspacing="0" width="100%">
                                  <thead>
                                    <tr>
                                      <th>Date</th>
                                      <th>Time</th>
                                      <th>Temprature</th>
                                      <th>Pulse</th>
                                      <th>Bp</th>
                                      <th>RR</th>
                                      <th>I/O</th>
                                      <th>RT Aspiration</th>
                                      <th>Treatment</th>
                                      <th>Nurse Name</th>
                                      
                                    </tr>
                                  </thead>
                                  @foreach($nursereviews as $nursereview)
                                        <tr>
                                          <td>{{$nursereview->date}}</td>
                                          <td>{{$nursereview->time}}</td>
                                          <td>{{$nursereview->temprature}}</td>
                                          <td>{{$nursereview->pulse}}</td>
                                          <td>{{$nursereview->bp}}</td>
                                          <td>{{$nursereview->resperatory_rate}}</td>
                                          <td>{{$nursereview->input_output}}</td>
                                          <td>{{$nursereview->rt_aspiration}}</td>
                                          <td>{{$nursereview->treatment}}</td>
                                          <td>{{$nursereview->nurse->name}}</td>
                                        </tr>
                                  @endforeach
                                  <tbody>
                                     
                                  </tbody>
                              </table>
                          </div>

                          <!--Doctor Review-->
                          <div class="x_title">
                                <h2>Doctor Review</h2>
                                <ul class="nav navbar-right panel_toolbox">
                                   <li><button type="button" class="btn btn-primary" id="addreview" data-toggle="modal" data-target="#doctormodal">Add</button></li>
                                </ul>   
                                <div class="clearfix"></div>
                          </div>
                              
                          <div class="x_content" id="doctorsreviewinfo" style="display: block;">

                              <table id="datatable-checkbox" class="table table-bordered table-fixed table-striped dt-responsive nowrap" cellspacing="0" width="100%">
                                  <thead>
                                    <tr>
                                      <th>Date</th>
                                      <th>Symptoms</th>
                                      <th>Findings</th>
                                      <th>Treatment</th>
                                      <th>Doctor Name</th>
                                      
                                    </tr>
                                  </thead>
                                  @foreach($doctorreviews as $doctorreview)
                                        <tr>
                                          <td>{{$doctorreview->date}}</td>
                                          <td>{{$doctorreview->symptoms}}</td>
                                          <td>{{$doctorreview->findings}}</td>
                                          <td>{{$doctorreview->treatment}}</td>
                                          <td>{{$doctorreview->doctor->name}}</td>

                                        </tr>
                                  @endforeach
                                  <tbody>
                                     
                                  </tbody>
                              </table>
                          </div>


                          </div>
                          
                        </div>
                      </div>
                       

                    </div>
                    </div>

                      <!-- Nurse Modal -->
                     <div class="modal fade" id="nursemodal" role="dialog">
                      <div class="modal-dialog">
                       <!-- Modal content-->
                       <form id="nursereview-form" data-parsley-validate="" class="form-horizontal form-label-left" novalidate="">
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Nurse Review</h4>
                          </div>
                          <div class="modal-body">
                                  

                                    <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
                                    <input class="form-control col-md-7 col-xs-12" id="date" size="16" type="hidden" name="data[ipdpetient_id]" value="{{$ipdpatient->regno}}" >

                                    <div class="form-group">
                                        <label for="treatment" class="control-label col-md-3 col-sm-3 col-xs-12">Select Nurse</label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">

                                          <select class="form-control" name="data[nurse_id]" id="regi_type" >
                                            @foreach($nurses as $nurse)
                                                <option value="{{$nurse->id}}" >{{$nurse->name}}</option>
                                            @endforeach
                                          </select>
                                          
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="phone" class="control-label col-md-3 col-sm-3 col-xs-12">Date</label>
                                        <div class="input-append date form_datetime col-md-6 col-sm-6 col-xs-12" data-date="2012-12-21T15:25:00Z">
                                            <input class="form-control col-md-7 col-xs-12" id="date" size="16" type="text" name="data[date]" value="" readonly>
                                           
                                            <span class="add-on"><i class="icon-remove"></i></span>
                                            <span class="add-on"><i class="icon-th"></i></span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="temprature" class="control-label col-md-3 col-sm-3 col-xs-12">Temprature</label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                          <input id="test_containt" class="form-control col-md-7 col-xs-12" type="text" name="data[temprature]">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="pulse" class="control-label col-md-3 col-sm-3 col-xs-12">Pulse</label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                          <input id="pulse" class="form-control col-md-7 col-xs-12" type="text" name="data[pulse]">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="bp" class="control-label col-md-3 col-sm-3 col-xs-12">BP</label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                          <input id="bp" class="form-control col-md-7 col-xs-12" type="text" name="data[bp]">
                                        </div>
                                    </div>
                                     <div class="form-group">
                                        <label for="bp" class="control-label col-md-3 col-sm-3 col-xs-12">RR (Resperatory Rate)</label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                          <input id="resperatory_rate" class="form-control col-md-7 col-xs-12" type="text" name="data[resperatory_rate]">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="bp" class="control-label col-md-3 col-sm-3 col-xs-12">I/O</label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                          <input id="input_output" class="form-control col-md-7 col-xs-12" type="text" name="data[input_output]">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="bp" class="control-label col-md-3 col-sm-3 col-xs-12">RT Aspiration</label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                          <input id="rt_aspiration" class="form-control col-md-7 col-xs-12" type="text" name="data[rt_aspiration]">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="treatment" class="control-label col-md-3 col-sm-3 col-xs-12">Treatment</label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                          <input id="treatment" class="form-control col-md-7 col-xs-12" type="text" name="data[treatment]">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="treatment" class="control-label col-md-3 col-sm-3 col-xs-12">Time</label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">

                                          <select class="form-control" name="data[time]" id="regi_type" >
                                            <option value="morning" >Morning</option>
                                            <option value="afternoon">AfterNoon</option>
                                            <option value="evening">Evening</option>
                                            <option value="night">Night</option>
                                          </select>
                                          
                                        </div>
                                    </div>


                                
                          </div>
                          <div class="modal-footer">
                            <button class="btn btn-primary" type="submit" id="save-test">Save</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                          </div>
                        </div>
                        </form>
                         </div>
                    </div>

                     <!-- Doctor Modal -->
                     <div class="modal fade" id="doctormodal" role="dialog">
                      <div class="modal-dialog">
                       <!-- Modal content-->
                       <form id="doctorsreview-form" data-parsley-validate="" class="form-horizontal form-label-left" novalidate="">
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Doctor Review</h4>
                          </div>
                          <div class="modal-body">
                                  

                                    <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
                                    <input class="form-control col-md-7 col-xs-12" id="date" size="16" type="hidden" name="data[ipdpetient_id]" value="{{$ipdpatient->regno}}" >

                                    <div class="form-group">
                                        <label for="treatment" class="control-label col-md-3 col-sm-3 col-xs-12">Select Nurse</label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">

                                          <select class="form-control" name="data[doctor_id]" id="regi_type" >
                                            @foreach($doctors as $doctor)
                                                <option value="{{$doctor->id}}" >{{$doctor->name}}</option>
                                            @endforeach
                                          </select>
                                          
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="date" class="control-label col-md-3 col-sm-3 col-xs-12">Date</label>
                                        <div class="input-append date form_datetime col-md-6 col-sm-6 col-xs-12" data-date="2012-12-21T15:25:00Z">
                                            <input class="form-control col-md-7 col-xs-12" id="date" size="16" type="text" name="data[date]" value="" readonly>
                                           
                                            <span class="add-on"><i class="icon-remove"></i></span>
                                            <span class="add-on"><i class="icon-th"></i></span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="symptoms" class="control-label col-md-3 col-sm-3 col-xs-12">Symptoms</label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                          <input id="symptoms" class="form-control col-md-7 col-xs-12" type="text" name="data[symptoms]">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="symptoms" class="control-label col-md-3 col-sm-3 col-xs-12">Findings</label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                          <input id="findings" class="form-control col-md-7 col-xs-12" type="text" name="data[findings]">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="treatment" class="control-label col-md-3 col-sm-3 col-xs-12">Treatment</label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                          <input id="treatment" class="form-control col-md-7 col-xs-12" type="text" name="data[treatment]">
                                        </div>
                                    </div>
                                
                          </div>
                          <div class="modal-footer">
                            <button class="btn btn-primary" type="submit" id="save-test">Save</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                          </div>
                        </div>
                        </form>
                         </div>
                    </div>


        @include('layout.footer')
      </div>
    </div>
@include('layout.scripts')
<script> var root="{{$root}}/";</script>

@include('layout.datatables_js')
<script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.js"></script>
<!-- <script>
    $(document).ready(function() {
      $('#summernote').summernote({
        placeholder: 'Please Enter Advice',
        tabsize: 2,
        height: 100
      });
      $('#summernote1').summernote({
        placeholder: 'Please Enter Prescriptions',
        tabsize: 2,
        height: 100
      });
    });
  </script> -->

<script type="text/javascript" src="public/js/bootstrap-datetimepicker.js" charset="UTF-8"></script>
<script type="text/javascript" src="public/js/bootstrap-datetimepicker.fr.js" charset="UTF-8"></script>
<script src="public/vendors/bootstrap-daterangepicker/daterangepicker.js"></script>
<script src="public/vendors/jquery.tagsinput/src/jquery.tagsinput.js"></script>
<script src="public/custom-js/ipdpetient.js"></script>

<script type="text/javascript">
 /* $(".form_datetime").datetimepicker({
        format: "yyyy-mm-dd hh:ii",
        showMeridian: true,
        autoclose: true,
        todayBtn: true
    });*/
</script>
   </body>
</html>
