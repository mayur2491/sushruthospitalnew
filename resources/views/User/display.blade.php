<!DOCTYPE html>
<html lang="en">
  <head>
        @include('layout.head') 
        <!-- <link href="public/vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet"> -->
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        @include('layout.left-sidebar')
        @include('layout.topnav')
        <div class="right_col" role="main">
                <div class="row">
                      <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                          <div class="x_title">
                            <h2>Users List</h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><button type="button" class="btn btn-primary" id="newpetient" data-toggle="modal" data-target="#usermodal">New User &nbsp;&nbsp;&nbsp;<i class="fa fa-plus"></i></button></li>
                            </ul>   
                            <div class="clearfix"></div>
                          </div>
                          <div class="x_content" style="display: block;">
                            <br>
                             <div class="panel-body">
                                <!-- <div id="data">

                                </div> -->
                                @if(isset($users) && count($users)>0)
                                  <table id="patientsdata" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%" >
                                      <thead>
                                      <tr>
                                          <td>Name</td>
                                          <td>UserName</td>
                                          <td>Password</td>
                                          <td>UserType</td>
                                          <td>Profile Image</td>
                                          <td>Action</td>

                                          
                                          <td><input type="checkbox" onclick="$('input[name*=\'selected\']').prop('checked', this.checked);"></td>
                                      </tr>
                                      </thead>
                                      <tbody>
                                      @foreach($users as $user)
                                      <tr>

                                          <td>{{$user->name}}</td>
                                          <td>{{$user->username}}</td>
                                          <td>{{$user->password}}</td>
                                          <td>{{$user->usertype}}</td>
                                          <td><img src="{{$user->profile_img}}" alt="..." class=" thumbnail" style="height: 77px;"></td>
                                          

                                          <td><a class="btn btn-primary edituser"  href="{{$root}}/edit-user?id={{$user->id}}"><i class="glyphicon glyphicon-pencil"></i></a></td>
                                          <td><input type="checkbox" name="selected" value="{{$user->id}}" class="deleteval"></td>
                                      </tr>

                                      @endforeach
                                      </tbody>
                                  </table>

                                  @else
                                  No users found...
                                  @endif
                                  {!! $users->render() !!}
                                
                            </div>
                             
                          </div>
                        </div>
                      </div>
               </div>
        </div>
        @include('layout.footer')

        <!-- Modal -->
  <div class="modal fade" id="usermodal" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">User Information</h4>
        </div>
        <div class="modal-body">
          <form id="user-form"  class="form-horizontal form-label-left" enctype="multipart/form-data">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
                                
                              <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Name <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                  <input type="text" id="name" required="required" class="form-control col-md-7 col-xs-12" name="data[name]">
                                </div>
                              </div>
                               <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="age">UserName<span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                  <input type="text" id="username" required="required" class="form-control col-md-7 col-xs-12" name="data[username]">
                                </div>
                              </div>
                              <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="address">Password<span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                  <input type="text" id="password" name="data[password]" required="required" class="form-control col-md-7 col-xs-12">
                                </div>
                              </div>
                              <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="address">Profile Image<span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                  <input type="file" id="profile_img" name="profile_img" class="form-control col-md-7 col-xs-12">
                                  <div class="profile_pic" id="profile_pic"><br>
                                    <img src="public/images/placeholder-image.png" alt="..." class="img-thumbnail profile_img" id="imag">
                                  </div>
                                </div>
                              </div>
                                                            
                              <div class="ln_solid"></div>
                              <div class="form-group">
                                <!-- <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3"> -->
                                  <button class="btn btn-primary" type="submit" id="save-user">Save</button>
                                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                </div>
                              </div>

            </form>
        </div>
        
      </div>
      
    </div>
  </div>

      </div>
    </div>
@include('layout.scripts')
<script> var root="{{$root}}/";</script>
<!-- <script src="public/vendors/bootstrap-daterangepicker/daterangepicker.js"></script>
<script src="public/vendors/jquery.tagsinput/src/jquery.tagsinput.js"></script> -->
<script src="public/custom-js/user.js"></script>


   </body>
</html>
