<!DOCTYPE html>
<html lang="en">
  <head>
        @include('layout.head') 
        <link href="public/css/bootstrap-datetimepicker.min.css" rel="stylesheet" media="screen">
        <link href="public/vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
        <link href="public/css/custom.css" rel="stylesheet">
        
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        @include('layout.left-sidebar')
        @include('layout.topnav')
        <div class="right_col" role="main">
                <div class="row">
                      <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                          <div class="x_title">
                            <h2>Nurse details</h2>
                            <div class="clearfix"></div>
                          </div>
                          <div id="message" style="display: none;">
                              
                          </div>
                          <div class="x_content" style="display: block;">
                            <br>
                            <form id="editnurseform" data-parsley-validate="" class="form-horizontal form-label-left" novalidate="">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
                                                             
                                <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Name <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                  <input type="text" id="name" required="required" class="form-control col-md-7 col-xs-12" name="data[name]" value="{{$nurse->name}}">
                                </div>
                              </div>
                               
                              <div class="form-group">
                                <label for="phone" class="control-label col-md-3 col-sm-3 col-xs-12">Contact</label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                  <input id="contact" class="form-control col-md-7 col-xs-12" type="text" name="data[contact]" value="{{$nurse->contact}}">
                                </div>
                              </div>
                              
                              <div class="control-group form-group ">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">education</label>
                                <div class="col-md-6 col-sm-9 col-xs-12">
                                  <input id="tags_1" type="text" name="data[education]" class="tags form-control" value="{{$nurse->education}}" />
                                  <div id="suggestions-container" style="position: relative; float: left; width: 250px; margin: 10px;"></div>
                                </div>
                              </div>

                              <div class="form-group">
                                  <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                     <button class="btn btn-primary" type="submit" id="update-ipdpatient">Update</button>
                                  </div>
                              </div>

                            </form>
                            <div class="ln_solid"></div>
                          </div>


                          </div>
                          
                        </div>
                      </div>
                       

                    </div>
                    </div>

        @include('layout.footer')
      </div>
    </div>
@include('layout.scripts')
<script> var root="{{$root}}/";</script>

<script type="text/javascript" src="public/js/bootstrap-datetimepicker.js" charset="UTF-8"></script>
<script type="text/javascript" src="public/js/bootstrap-datetimepicker.fr.js" charset="UTF-8"></script>
<script src="public/vendors/bootstrap-daterangepicker/daterangepicker.js"></script>
<script src="public/vendors/jquery.tagsinput/src/jquery.tagsinput.js"></script>
<script src="public/custom-js/nurse.js"></script>
<script type="text/javascript">
  $(".form_datetime").datetimepicker({
        format: "yyyy-mm-dd hh:ii",
        showMeridian: true,
        autoclose: true,
        todayBtn: true
    });
</script>
   </body>
</html>
