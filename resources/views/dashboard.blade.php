<!DOCTYPE html>
<html lang="en">
  <head>
		@include('layout.head')    
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        @include('layout.left-sidebar')
		@include('layout.topnav')
		@include('layout.pagecontent')
		@include('layout.footer')
      </div>
    </div>
@include('layout.scripts')
    
	
  </body>
</html>
