<!DOCTYPE html>
<html lang="en">
  <head>
        @include('layout.head') 
        <link href="public/vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">

        <!-- Datatables-->
        <link href="public/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
        <link href="public/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
        <link href="public/vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
        <link href="public/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
        <link href="public/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">
        <!--Datatables -->
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        @include('layout.left-sidebar')
        @include('layout.topnav')

        <div class="right_col" role="main">
                <div class="row">
                      <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                          <div class="x_title">
                            <h2>Doctors List</h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><button type="button" class="btn btn-primary" id="newpetient" data-toggle="modal" data-target="#DoctorFormModal">New Doctor &nbsp;&nbsp;&nbsp;<i class="fa fa-plus"></i></button></li>
                            </ul>   
                            <div class="clearfix"></div>
                          </div>
                          <div id="message" style="display: none;">
                              
                          </div>
                          <div class="x_content" style="display: block;">
                            <br>
                             <div class="panel-body">
                                
                                @if(isset($doctors) && count($doctors)>0)
                                  <table id="datatable" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%" >
                                      <thead>
                                      <tr>
                                          <td><input type="checkbox" onclick="$('input[name*=\'selected\']').prop('checked', this.checked);"></td>
                                          <td>Name</td>
                                          <td>Contact</td>
                                          <td>Education</td>
                                          <td>Action</td>
                                          
                                      </tr>
                                      </thead>
                                      <tbody id="doctorlist-tbody">
                                      @foreach($doctors as $doctor)
                                      <tr>
                                          <td><input type="checkbox" name="selected" value="{{$doctor->id}}" class="deleteval"></td>
                                          <td>{{$doctor->name}}</td>
                                          <td>{{$doctor->contact}}</td>
                                          <td>{{$doctor->education}}</td>

                                          <td><a class="btn btn-primary edituser btn-xs"  href="{{$root}}/edit-doctor?id={{$doctor->id}}"><i class="glyphicon glyphicon-pencil"></i></a></td>
                                          
                                      </tr>

                                      @endforeach
                                      </tbody>
                                  </table>

                                  @else
                                  No users found...
                                  @endif
                                 
                                
                            </div>
                             
                          </div>
                        </div>
                      </div>
               </div>
        </div>
        @include('layout.footer')

        <!-- Modal -->
  <div class="modal fade" id="DoctorFormModal" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Doctor Information</h4>
        </div>
        <div class="modal-body">
          <form id="doctor-form"  class="form-horizontal form-label-left">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
                                
                             <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Name <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                  <input type="text" id="name" required="required" class="form-control col-md-7 col-xs-12" name="data[name]">
                                </div>
                            </div>
                               
                            <div class="form-group">
                              <label for="phone" class="control-label col-md-3 col-sm-3 col-xs-12">Contact</label>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                <input id="contact" class="form-control col-md-7 col-xs-12" type="text" name="data[contact]">
                              </div>
                            </div>
                              
                            <div class="control-group form-group ">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12">education</label>
                              <div class="col-md-6 col-sm-9 col-xs-12">
                                <input id="tags_1" type="text" name="data[education]" class="tags form-control" value="" />
                                <div id="suggestions-container" style="position: relative; float: left; width: 250px; margin: 10px;"></div>
                              </div>
                            </div>
                              
                              <div class="ln_solid"></div>
                              <div class="form-group">
                                <!-- <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3"> -->
                                  <button class="btn btn-primary" type="submit" id="save-doctor">Save</button>
                                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                              </div>
                              </div>

            </form>
        </div>
        
      </div>
      
    </div>
  </div>

      </div>
    </div>
@include('layout.scripts')
<script> var root="{{$root}}/";</script>
<script src="public/vendors/bootstrap-daterangepicker/daterangepicker.js"></script>
<script src="public/vendors/jquery.tagsinput/src/jquery.tagsinput.js"></script>


<!-- datatables -->
    <script src="public/vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="public/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="public/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="public/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="public/vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="public/vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="public/vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="public/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="public/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="public/vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="public/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="public/vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
    <!-- Data tables-->


<script src="public/custom-js/doctor.js"></script>


   </body>
</html>
