<!DOCTYPE html>
<html lang="en">
  <head>
        @include('layout.head') 
        <link href="public/vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
        <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/css/bootstrap-datepicker.css" rel="stylesheet">  
        
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        @include('layout.left-sidebar')
        @include('layout.topnav')
        <div class="right_col" role="main">
                <div class="row">
                      <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                          <div class="x_title">
                            <h2>Patients Catlogue</h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><button type="button" class="btn btn-primary" id="newpetient" data-toggle="modal" data-target="#myModal">New Petient &nbsp;&nbsp;&nbsp;<i class="fa fa-plus"></i></button></li>
                            </ul>   
                            <div class="clearfix"></div>
                          </div>
                          <div class="x_content" style="display: block;">
                            <br>

                             <div class="panel-body">
                                
                                @if(isset($patients) && count($patients)>0)
                                  <table id="patientsdata" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%" >
                                      <thead>
                                      <tr>
                                          <td>Name</td>
                                          <td>Address</td>
                                          <td>Contact</td>
                                          <td>Gender</td>
                                          <td>Date of birth</td>
                                          <td>Advice</td>
                                          <td>Action</td>
                                          <td><input type="checkbox" onclick="$('input[name*=\'selected\']').prop('checked', this.checked);"></td>
                                      </tr>
                                      </thead>
                                      <tbody>
                                      @foreach($patients as $patient)
                                      <tr>

                                          <td>{{$patient->name}}</td>
                                          <td>{{$patient->address}}</td>
                                          <td>{{$patient->contact}}</td>
                                          <td>{{$patient->gender}}</td>
                                          <td>{{$patient->date_of_birth}}</td>
                                          <td>{{$patient->advice}}</td>

                                          <td><a class="btn btn-primary edituser"  href="{{$root}}/edit?petient_id={{$patient->id}}"><i class="glyphicon glyphicon-pencil"></i></a></td>
                                          <td><input type="checkbox" name="selected" value="{{$patient->id}}" class="deleteval"></td>
                                      </tr>

                                      @endforeach
                                      </tbody>
                                  </table>

                                  @else
                                  No users found...
                                  @endif
                                  {!! $patients->render() !!}
                            </div>
                             
                          </div>
                        </div>
                      </div>
               </div>
        </div>
        @include('layout.footer')

        <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Patient Information</h4>
        </div>
        <div class="modal-body">
          <form id="patient-form"  class="form-horizontal form-label-left">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
                              <!-- <div class="x_title">
                                <h4>Basic Information</h4>
                                <div class="clearfix"></div>
                              </div> -->
                                
                              <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Name <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                  <input type="text" id="name" required="required" class="form-control col-md-7 col-xs-12" name="data[name]">
                                </div>
                              </div>
                               <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="age">Age <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                  <input type="text" id="age" required="required" class="form-control col-md-7 col-xs-12" name="data[age]">
                                </div>
                              </div>
                              <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="address">Address <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                  <input type="text" id="address" name="data[address]" required="required" class="form-control col-md-7 col-xs-12">
                                </div>
                              </div>
                              <div class="form-group">
                                <label for="phone" class="control-label col-md-3 col-sm-3 col-xs-12">Contact</label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                  <input id="contact" class="form-control col-md-7 col-xs-12" type="text" name="data[contact]">
                                </div>
                              </div>
                              <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Gender</label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                  <div id="gender" class="btn-group" data-toggle="buttons">
                                    <label class="btn btn-default" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
                                      <input type="radio" name="data[gender]" value="male" data-parsley-multiple="gender"> &nbsp; Male &nbsp;
                                    </label>
                                    <label class="btn btn-primary" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
                                      <input type="radio" name="data[gender]" value="female" data-parsley-multiple="gender"> Female
                                    </label>
                                  </div>
                                </div>
                              </div>
                              <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Date Of Birth <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                  
                                  
                                    <input type="text" class="form-control has-feedback-left col-md-7 col-xs-12" id="single_cal1" placeholder="First Name" aria-describedby="inputSuccess2Status" name="data[date_of_birth]" required="required">
                                    <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                                    <span id="inputSuccess2Status" class="sr-only">(success)</span>
                                  

                                  <!-- <input id="birthday" class="date-picker form-control col-md-7 col-xs-12" required="required" type="text" name="data[date_of_birth]" data-inputmask="'mask': '99/99/9999'"> -->
                                </div>
                              </div>
                              <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="ref-by">Ref.By <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                  <input type="text" id="refby" required="required" class="form-control col-md-7 col-xs-12" name="data[refby]">
                                </div>
                              </div>
                              <div class="ln_solid"></div>
                              <div class="form-group">
                                <!-- <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3"> -->
                                  <button class="btn btn-primary" type="submit" id="save-patient">Save</button>
                                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                              </div>
                              </div>

            </form>
        </div>
        <!-- <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div> -->

      </div>
      
    </div>
  </div>

      </div>
    </div>
@include('layout.scripts')
<script> var root="{{$root}}/";</script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/js/bootstrap-datepicker.js"></script>
<script src="public/vendors/bootstrap-daterangepicker/daterangepicker.js"></script>
<script src="public/custom-js/patient.js"></script>

 <script type="text/javascript">
            // When the document is ready
            $(document).ready(function () {
                
                $('#select-year').datepicker({
                    minViewMode: 'years',
                    autoclose: true,
                    showMeridian: true,
                     format: 'yyyy'
                });  
                $('#select-month').datepicker({
                    minViewMode: 1,
                    autoclose: true,
                    showMeridian: true,
                     format: 'MM-yyyy'
                });  
            
            });
        </script>
   </body>
</html>
