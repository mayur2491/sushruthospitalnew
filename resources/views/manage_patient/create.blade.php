<!DOCTYPE html>
<html lang="en">
  <head>
        @include('layout.head') 
        
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        @include('layout.left-sidebar')
        @include('layout.topnav')
        <div class="right_col" role="main">
                <div class="row">
                      <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                          <div class="x_title">
                            <h2>Patient details</h2>
                            <div class="clearfix"></div>
                          </div>
                          <div class="x_content" style="display: block;">
                            <br>
                            <form id="patient-form" data-parsley-validate="" class="form-horizontal form-label-left" novalidate="">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
                              <div class="x_title">
                                <h4>Basic Information</h4>
                                <div class="clearfix"></div>
                              </div>
                                
                              <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Name <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                  <input type="text" id="name" required="required" class="form-control col-md-7 col-xs-12" name="data[name]">
                                </div>
                              </div>
                              <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="age">Age <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                  <input type="text" id="age" required="required" class="form-control col-md-7 col-xs-12" name="data[age]">
                                </div>
                              </div>
                              <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="address">Address <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                  <input type="text" id="address" name="data[address]" required="required" class="form-control col-md-7 col-xs-12">
                                </div>
                              </div>
                              <div class="form-group">
                                <label for="phone" class="control-label col-md-3 col-sm-3 col-xs-12">Contact</label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                  <input id="contact" class="form-control col-md-7 col-xs-12" type="text" name="data[contact]">
                                </div>
                              </div>
                              <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Gender</label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                  <div id="gender" class="btn-group" data-toggle="buttons">
                                    <label class="btn btn-default" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
                                      <input type="radio" name="data[gender]" value="male" data-parsley-multiple="gender"> &nbsp; Male &nbsp;
                                    </label>
                                    <label class="btn btn-primary" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
                                      <input type="radio" name="data[gender]" value="female" data-parsley-multiple="gender"> Female
                                    </label>
                                  </div>
                                </div>
                              </div>
                              <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Date Of Birth <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                  <input id="birthday" class="date-picker form-control col-md-7 col-xs-12" required="required" type="text" name="data[date_of_birth]">
                                </div>
                              </div>
                              <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="ref-by">Ref.By <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                  <input type="text" id="refby" required="required" class="form-control col-md-7 col-xs-12" name="data[refby]">
                                </div>
                              </div>
                              <div class="ln_solid"></div>
                              <div class="form-group">
                                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                  <button class="btn btn-primary" type="submit" id="save-patient">Save</button>
                                </div>
                              </div>

                            </form>
                          </div>
                        </div>
                      </div>
                    </div>
                    </div>
        @include('layout.footer')
      </div>
    </div>
@include('layout.scripts')
<script> var root="{{$root}}/";</script>
<script src="public/custom-js/patient.js"></script>
   </body>
</html>
