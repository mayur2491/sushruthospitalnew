<!DOCTYPE html>
<html lang="en">
  <head>
        @include('layout.head') 
        <link href="public/css/bootstrap-datetimepicker.min.css" rel="stylesheet" media="screen">
        <link href="public/vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
        <link href="public/css/custom.css" rel="stylesheet">
        
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        @include('layout.left-sidebar')
        @include('layout.topnav')
        <div class="right_col" role="main">
                <div class="row">
                      <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                          <div class="x_title">
                            <h2>Patient details</h2>
                            <div class="clearfix"></div>
                          </div>

                          <div class="x_content" style="display: block;">
                            <br>
                            <form id="editform" data-parsley-validate="" class="form-horizontal form-label-left" novalidate="">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
                             
                              <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Name <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                  <input type="text" id="name" required="required" class="form-control col-md-7 col-xs-12" name="data[name]" value="{{$patient->name}}">
                                </div>
                              </div>
                              <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Age <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                  <input type="text" id="name" required="required" class="form-control col-md-7 col-xs-12" name="data[age]" value="{{$patient->age}}">
                                </div>
                              </div>
                              <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="address">Address <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                  <input type="text" id="address" name="data[address]" required="required" class="form-control col-md-7 col-xs-12" value="{{$patient->address}}">
                                </div>
                              </div>
                              <div class="form-group">
                                <label for="phone" class="control-label col-md-3 col-sm-3 col-xs-12">Contact</label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                  <input id="contact" class="form-control col-md-7 col-xs-12" type="text" name="data[contact]" value="{{$patient->contact}}">
                                </div>
                              </div>
                              <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Gender</label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                  <div id="gender" class="btn-group" data-toggle="buttons">
                                    
                                    @if($patient->gender=="male")

                                    <label class="btn btn-primary active "  data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
                                      <input type="radio" name="data[gender]" value="male" data-parsley-multiple="gender"> &nbsp; Male &nbsp;
                                    </label>
                                    <label class="btn btn-default" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
                                      <input type="radio" name="data[gender]" value="female" data-parsley-multiple="gender"> Female
                                    </label>
                                    @else
                                    <label class="btn btn-default "  data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
                                      <input type="radio" name="data[gender]" value="male" data-parsley-multiple="gender"> &nbsp; Male &nbsp;
                                    </label>
                                    <label class="btn btn-primary active" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
                                      <input type="radio" name="data[gender]" value="female" data-parsley-multiple="gender"> Female
                                    </label>
                                    @endif
                                  </div>
                                </div>
                              </div>

                              <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Date Of Birth <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" class="form-control has-feedback-left col-md-7 col-xs-12" id="single_cal1" placeholder="First Name" aria-describedby="inputSuccess2Status" name="data[date_of_birth]" value="{{$patient->date_of_birth}}"required="required">
                                    <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                                    <span id="inputSuccess2Status" class="sr-only">(success)</span>
                                </div>
                              </div>

                              <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Ref.By <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                  <input type="text" id="name" required="required" class="form-control col-md-7 col-xs-12" name="data[refby]" value="{{$patient->refby}}">
                                </div>
                              </div>
                              <!-- <div class="form-group" id="descese_field"><button class="btn btn-primary" type="button" onclick="addField()"><i class="fa fa-plus"></i></button>
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Descese 
                                  <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12 ">
                                  <input id="descese_name" class="form-control col-md-7 col-xs-12" type="text" name="descese_name[]">
                                </div>
                              </div> -->

                              <div class="control-group form-group ">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Dignosis</label>
                                <div class="col-md-6 col-sm-9 col-xs-12">
                                  <input id="tags_1" type="text" name="data[descese_name]" class="tags form-control" value="{{$patient->descese_name}}" />
                                  <div id="suggestions-container" style="position: relative; float: left; width: 250px; margin: 10px;"></div>
                                </div>
                              </div>



                              <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Advice<span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">

                                  <select class="form-control" name="data[advice]">
                                    <option values="{{$patient->advice}}" hidden>{{strtoupper($patient->advice)}}</option>
                                    <option value="ipd">IPD</option>
                                    <option value="opd">OPD</option>
                                  </select>
                                </div>
                              </div>
                              @if(strtolower($patient->advice)=='ipd')
                              <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">IPD Form Link <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                 
                                 @if($flag==1)
                                 <button type="button" class="btn btn-link not_active" id="desceseinfo" data-toggle="modal" data-target="#ipdmodal"> IPD form is submited</button>    <i class="fa fa-check tick_icon"></i>
                                 @else
                                 <button type="button" class="btn btn-link " id="desceseinfo" data-toggle="modal" data-target="#ipdmodal">Please click here for IPD form</button>
                                 @endif

                                </div>
                              </div>
                              
                              @endif
                              <div class="form-group">
                                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                  <button class="btn btn-primary" type="submit" id="save-patient">Update</button>
                                </div>
                              </div>

                            </form>
                            <div class="ln_solid"></div>
                          </div>


                          

                          <!-- Test information -->

                          <div class="x_title">
                                    <h2>Investigation Information</h2>
                                    <ul class="nav navbar-right panel_toolbox">
                                       <li><button type="button" class="btn btn-primary" id="addtest" data-toggle="modal" data-target="#testmodal">Add</button></li>
                                    </ul>   
                                    <div class="clearfix"></div>
                              </div>
                                  
                              <div class="x_content" id="testinfo" style="display: block;">

                                  <table id="datatable-responsive" class="table table-striped dt-responsive nowrap" cellspacing="0" width="100%">
                                      <thead>
                                        <tr>
                                          <th>Test Name</th>
                                          <th>Test Containt</th>
                                          <th>Test Report</th>
                                        </tr>
                                      </thead >
                                      <tbody id="test-tbody">
                                         @foreach($tests as $test)
                                        <tr>
                                          <td>{{$test->test_name}}</td>
                                          <td>{{$test->test_containt}}</td>
                                          <td>{{$test->test_report}}</td>
                                        </tr>
                                        @endforeach
                                        </tbody>
                                  </table>
                              </div>

                          </div>

                        </div>
                      </div>

                    </div>
                    </div>



                    <!-- Modal -->
                    <div class="modal fade" id="descesemodal" role="dialog">
                      <div class="modal-dialog">
                        <!-- Modal content-->
                        <form id="descese-form" data-parsley-validate="" class="form-horizontal form-label-left" novalidate="">
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Deceses Form</h4>
                          </div>
                          <div class="modal-body">
                              
                                   <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
                                    <div class="form-group">
                                      <label for="phone" class="control-label col-md-3 col-sm-3 col-xs-12">Descese name</label>
                                      <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input id="descese_name" class="form-control col-md-7 col-xs-12" type="text" name="data[descese_name]">
                                      </div>
                                    </div>
                                    <input type="hidden" name="data[petient_id]" value="{{$patient->id}}"/>
                                
                          </div>
                          <div class="modal-footer">
                            <button class="btn btn-primary" type="submit" id="save-descese">Save</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                          </div>
                        </div>
                        </form>
                      </div>
                    </div>

                    <div class="modal fade" id="prevtreatmentmodal" role="dialog">
                      <div class="modal-dialog">
                      <form id="prevtreatment-form" data-parsley-validate="" class="form-horizontal form-label-left" novalidate="">
                        <!-- Modal content-->
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Previous Treatment Form</h4>
                          </div>
                          <div class="modal-body">
                              
                                   <input type="hidden" name="_token" value="{{ csrf_token() }}"/>

                                  <div class="form-group">
                                    <label for="phone" class="control-label col-md-3 col-sm-3 col-xs-12">Doctor Name</label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                      <input id="phone" class="form-control col-md-7 col-xs-12" type="text" name="data[doctor_name]">
                                  </div>
                                  </div>
                                  <div class="form-group">
                                    <label for="phone" class="control-label col-md-3 col-sm-3 col-xs-12">Doctor Address</label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                      <input id="phone" class="form-control col-md-7 col-xs-12" type="text" name="data[doctor_address]">
                                    </div>
                                  </div>
                                  <div class="form-group">
                                    <label for="phone" class="control-label col-md-3 col-sm-3 col-xs-12">Doctor Contact no.</label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                      <input id="phone" class="form-control col-md-7 col-xs-12" type="text" name="data[doctor_contact]">
                                    </div>
                                  </div>
                                  <div class="form-group">
                                    <label for="phone" class="control-label col-md-3 col-sm-3 col-xs-12">Final Report</label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                      <input id="phone" class="form-control col-md-7 col-xs-12" type="text" name="data[final_report]">
                                    </div>
                                  </div>

                                
                          </div>
                          <div class="modal-footer">
                            <button class="btn btn-primary" type="submit" id="save-patient">Save</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                          </div>
                        </div>
                        </form>

                         </div>
                    </div>

                    <div class="modal fade" id="testmodal" role="dialog">
                      <div class="modal-dialog">
                       <!-- Modal content-->
                       <form id="test-form" data-parsley-validate="" class="form-horizontal form-label-left" novalidate="">
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Petient Tests Information</h4>
                          </div>
                          <div class="modal-body">
                                  
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
                                    
                                    <div class="form-group">  
                                      <label for="phone" class="control-label col-md-3 col-sm-3 col-xs-12">Test name</label>
                                      <div class="col-md-6 col-sm-6 col-xs-12">
                                        <select class="form-control" name="data[test_name]" id="test_name" onchange="">
                                          <option hidden >Select Test</option>
                                          <option value="laboratory">Laboratory</option>
                                          <option value="xray">Xray</option>
                                          <option value="usg">USG</option>
                                          <option value="ctscan">C T Scan</option>
                                          <option value="mri-report">MRI Report</option>
                                          <option value="auto">others</option>
                                        </select>
                                      </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="phone" class="control-label col-md-3 col-sm-3 col-xs-12">Test containt</label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                          <input id="test_containt" class="form-control col-md-7 col-xs-12" type="text" name="data[test_containt]">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="phone" class="control-label col-md-3 col-sm-3 col-xs-12">Test Report</label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                          <input id="test_report" class="form-control col-md-7 col-xs-12" type="text" name="data[test_report]">
                                        </div>
                                    </div>
                                  

                                
                          </div>
                          <div class="modal-footer">
                            <button class="btn btn-primary" type="submit" id="save-test">Save</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                          </div>
                        </div>
                        </form>
                         </div>
                    </div>



                    <!-- IPD form modal start-->

                    <div class="modal fade" id="ipdmodal" role="dialog">
                      <div class="modal-dialog modal-lg">
                      <form id="ipd-form" data-parsley-validate="" class="form-horizontal form-label-left" novalidate="">
                        <!-- Modal content-->
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">IPD Form</h4>
                          </div>
                          <div class="modal-body">
                              <div class="row">
                                   <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
                                   <input type="hidden" name="data[petient_id]" value="{{$patient->id}}"/>

                                  <div class=" col-md-6 col-sm-12 col-xs-12 form-group">
                                    <!-- <label for="name" class="control-label col-md-2 col-sm-3 col-xs-12">Name</label> -->
                                    <div class="col-md-12 col-sm-6 col-xs-12">
                                      
                                      <input id="name" class="form-control col-md-7 col-xs-12" type="text" value="{{$patient->name}}" placeholder="Name" disabled="disabled">
                                  </div>
                                  </div>
                                  
                                  
                                  <div class="col-md-6 col-sm-12 col-xs-12 form-group">
                                    <!-- <label for="age" class="control-label col-md-2 col-sm-3 col-xs-12">Age</label> -->
                                    <div class="col-md-12 col-sm-6 col-xs-12">
                                      
                                      <input id="age" class="form-control col-md-7 col-xs-12" type="text" value="{{$patient->age}}" placeholder="Age" disabled="disabled">
                                    </div>
                                  </div>
                                  <div class="col-md-6 col-sm-12 col-xs-12 form-group">
                                    <!-- <label for="address" class="control-label col-md-2 col-sm-3 col-xs-12">Address</label> -->
                                    <div class="col-md-12 col-sm-6 col-xs-12">
                                      
                                      <input id="address" class="form-control col-md-7 col-xs-12" type="text" value="{{$patient->address}}" placeholder="Address" disabled="disabled">
                                    </div>
                                  </div>

                                  <div class=" col-md-6 col-sm-12 col-xs-12 form-group">
                                    <!-- <label for="address" class="control-label col-md-2 col-sm-3 col-xs-12">Gender</label> -->
                                    <div class="col-md-12 col-sm-6 col-xs-12">
                                      
                                      <input id="address" class="form-control col-md-7 col-xs-12" type="text" value="{{$patient->gender}}" placeholder="Gender" disabled="disabled">
                                    </div>
                                  </div>
                                  <div class="col-md-6 col-sm-12 col-xs-12 form-group">
                                    <!-- <label for="address" class="control-label col-md-3 col-sm-3 col-xs-12">Ref Doctor</label> -->
                                    <div class="col-md-12 col-sm-6 col-xs-12">
                                      
                                      <input id="address" class="form-control col-md-7 col-xs-12" type="text" value="{{$patient->refby}}" placeholder= "Ref Doctor" disabled="disabled">
                                    </div>
                                  </div>

                                  <div class="col-md-6 col-sm-12 col-xs-12 form-group">
                                    <!-- <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">RegNo<span class="required">*</span>
                                    </label> -->
                                    <div class="col-md-12 col-sm-6 col-xs-12" id="regi_type_div">
                                      <select class="form-control" name="regi_type" id="regi_type" onchange="change_reg_type()">
                                        <option hidden >Select Registration type</option>
                                        <option value="auto">Auto Registration No</option>
                                        <option value="manual">Manual Registration No</option>
                                      </select>
                                      <input id="regno" class="form-control col-md-7 col-xs-12" type="text" name="data[regno]" style="display: none;">
                                    </div>
                                  </div>

                                  <div class="col-md-6 col-sm-12 col-xs-12 form-group">
                                    <!-- <label for="occupation" class="control-label col-md-3 col-sm-3 col-xs-12">Occupation</label> -->
                                    <div class="col-md-12 col-sm-6 col-xs-12">
                                      <input id="occupation" class="form-control col-md-7 col-xs-12" type="text" placeholder="Occupation" name="data[occupation]">
                                    </div>
                                  </div>
                                  <div class="col-md-6 col-sm-12 col-xs-12 form-group">
                                    <!-- <label for="address" class="control-label col-md-3 col-sm-3 col-xs-12">Next Of Kins Name</label> -->
                                    <div class="col-md-12 col-sm-6 col-xs-12">
                                      <input id="next_kins_name" class="form-control col-md-7 col-xs-12" type="text" placeholder="Next Of Kins Name" name="data[next_kins_name]">
                                    </div>
                                  </div>
                                  <div class="col-md-6 col-sm-12 col-xs-12 form-group">
                                    <!-- <label for="address" class="control-label col-md-3 col-sm-3 col-xs-12">Kins Adderess</label> -->
                                    <div class="col-md-12 col-sm-6 col-xs-12">
                                      <input id="kins_address" class="form-control col-md-7 col-xs-12" type="text" placeholder="Kins Adderess" name="data[kins_address]">
                                    </div>
                                  </div>
                                  <div class="col-md-6 col-sm-12 col-xs-12 form-group">
                                    <div class="col-md-12 col-sm-6 col-xs-12">
                                      <input id="kins_contact" class="form-control col-md-7 col-xs-12" type="text" placeholder="Kins Contact" name="data[kins_contact]">
                                    </div>
                                  </div>

                                <div class="col-md-6 col-sm-12 col-xs-12 form-group">
                                  <!-- <label for="address" class="control-label col-md-3 col-sm-3 col-xs-12">Admited on Date Time</label> -->
                                  <div class="input-append date form_datetime col-md-12 col-sm-6 col-xs-12" data-date="2012-12-21T15:25:00Z">
                                      <input class="form-control col-md-7 col-xs-12" id="admited_datetime" size="16" type="text" name="data[admited_datetime]" placeholder="Admited on Date Time" value="" readonly>
                                      <!-- <span class="input-group-addon"><span class="glyphicon glyphicon-remove"></span></span>
                                          <span class="input-group-addon"><span class="glyphicon glyphicon-th"></span></span> -->
                                      <span class="add-on"><i class="icon-remove"></i></span>
                                      <span class="add-on"><i class="icon-th"></i></span>
                                  </div>
                                </div>

                                  
                                  <div class="col-md-6 col-sm-12 col-xs-12 form-group">
                                  <!-- <label for="address" class="control-label col-md-3 col-sm-3 col-xs-12">Discharge on Date Time</label> -->
                                  <div class="input-append date form_datetime col-md-12 col-sm-6 col-xs-12" data-date="2012-12-21T15:25:00Z">
                                      <input class="form-control col-md-7 col-xs-12" id="discharge_datetime" size="16" type="text" name="data[discharge_datetime]"  placeholder="Discharge on Date Time" value="" readonly>
                                      <!-- <span class="input-group-addon"><span class="glyphicon glyphicon-remove"></span></span>
                                          <span class="input-group-addon"><span class="glyphicon glyphicon-th"></span></span> -->
                                      <span class="add-on"><i class="icon-remove"></i></span>
                                      <span class="add-on"><i class="icon-th"></i></span>
                                  </div>
                                </div>
                                  <div class="col-md-6 col-sm-12 col-xs-12 form-group">
                                    
                                    <div class="col-md-12 col-sm-6 col-xs-12">
                                      <input id="ward_no" class="form-control col-md-7 col-xs-12" type="text" placeholder="Ward No" name="data[ward_no]">
                                    </div>
                                  </div>

                                  <div class="col-md-6 col-sm-12 col-xs-12 form-group" >
                                    <!-- <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">RegNo<span class="required">*</span>
                                    </label> -->
                                    <div class="col-md-12 col-sm-6 col-xs-12 category_div" id="category_div">
                                      <select class="form-control" name="data[is_mlc]" id="category" onchange="getMlcCount()">
                                        <option hidden >Select Category</option>
                                        <option value="1">MLC</option>
                                        <option value="0">Non MLC</option>
                                      </select>
                                      
                                    </div>
                                    <div class="col-md-3" id="mlccount" style="display:none;"></div>
                                  </div>

                                  <div class="col-md-6 col-sm-12 col-xs-12 form-group">
                                    <!-- <label for="operative_and_dignostic_procedure" class="control-label col-md-3 col-sm-3 col-xs-12">Operative and Dignostic Procedure Carried Out</label> -->
                                    <div class="col-md-12 col-sm-6 col-xs-12">
                                    <textarea class="resizable_textarea form-control" id="operative_and_dignostic_procedure" name="data[operative_and_dignostic_procedure]" placeholder="Operative and dignostic procedure"></textarea>
                                    </div>
                                  </div>

                                  <div class="col-md-6 col-sm-12 col-xs-12 form-group">
                                    <!-- <label for="address" class="control-label col-md-3 col-sm-3 col-xs-12">HPR Report</label> -->
                                    <div class="col-md-12 col-sm-6 col-xs-12">
                                      <textarea class="resizable_textarea form-control" id="hr_report" name="data[hr_report]" placeholder="HR Report"></textarea>
                                    </div>
                                  </div>
                                
                          </div>
                        </div>
                          <div class="modal-footer">
                            <button class="btn btn-primary" type="submit" id="save-ipdpatient">Save</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                          </div>
                        </div>
                        </form>

                         </div>
                    </div>

                    <!-- IPD form modal end -->

                    <div class="modal fade" id="afterbeforevideosmodal" role="dialog">
                      <div class="modal-dialog">
                      
                        <!-- Modal content-->
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">After and Before Videos</h4>
                          </div>
                          <div class="modal-body">
                                  <form id="test-form" data-parsley-validate="" class="form-horizontal form-label-left" novalidate="">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
                                    <div class="form-group">
                                      <label for="phone" class="control-label col-md-3 col-sm-3 col-xs-12">Before OPD Videos</label>
                                      <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input id="phone" class="form-control col-md-7 col-xs-12" type="text" name="phone">
                                      </div>
                                    </div>
                                    <div class="form-group">
                                      <label for="phone" class="control-label col-md-3 col-sm-3 col-xs-12">After OPD Videos</label>
                                      <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input id="phone" class="form-control col-md-7 col-xs-12" type="text" name="phone">
                                      </div>
                                    </div>
                                  </form>
                          </div>
                          <div class="modal-footer">
                            <button class="btn btn-primary" type="submit" id="save-test">Save</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                          </div>
                        </div>  
                      </div>
                    </div>

        @include('layout.footer')
      </div>
    </div>
@include('layout.scripts')
<script> var root="{{$root}}/";</script>

<script type="text/javascript" src="public/js/bootstrap-datetimepicker.js" charset="UTF-8"></script>
<script type="text/javascript" src="public/js/bootstrap-datetimepicker.fr.js" charset="UTF-8"></script>
<script src="public/vendors/bootstrap-daterangepicker/daterangepicker.js"></script>
<script src="public/vendors/jquery.tagsinput/src/jquery.tagsinput.js"></script>
<script src="public/custom-js/patient.js"></script>
<script type="text/javascript">
  $(".form_datetime").datetimepicker({
        format: "yyyy-mm-dd hh:ii",
        showMeridian: true,
        autoclose: true,
        todayBtn: true
    });
</script>
   </body>
</html>
