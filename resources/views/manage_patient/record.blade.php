
@if(isset($patients) && count($patients)>0)
<table id="patientsdata" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%" >
    <thead>
    <tr>
        <td>Name</td>
        <td>Address</td>
        <td>Contact</td>
        <td>Gender</td>
        <td>Date of birth</td>
        <td>Advice</td>
        <td>Action</td>
        <td><input type="checkbox" onclick="$('input[name*=\'selected\']').prop('checked', this.checked);"></td>
    </tr>
    </thead>
    <tbody>
    @foreach($patients as $patient)
    <tr>

        <td>{{$patient->name}}</td>
        <td>{{$patient->address}}</td>
        <td>{{$patient->contact}}</td>
        <td>{{$patient->gender}}</td>
        <td>{{$patient->date_of_birth}}</td>
        <td>{{$patient->advice}}</td>

        <td><a class="btn btn-primary edituser"  href="{{$root}}/edit?petient_id={{$patient->id}}"><i class="glyphicon glyphicon-pencil"></i></a></td>
        <td><input type="checkbox" name="selected" value="{{$patient->id}}" class="deleteval"></td>
    </tr>

    @endforeach
    </tbody>
</table>

@else
No users found...
@endif

<script src="public/custom-js/patient.js"></script>
<script>
    $(document).ready(function(){
        $('#patientsdata').DataTable({
            "bFilter": false , "bLengthChange": false ,
            dom: 'Bfrtip',
            buttons: [
                {
                    extend: 'csvHtml5',
                    exportOptions: { columns: [0,1,2,3] }
                },
                {
                    extend: 'excelHtml5',
                    exportOptions: {columns: [0,1,2,3] }
                }

            ]

        });
    });
</script>
