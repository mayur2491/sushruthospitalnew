<!DOCTYPE html>
<html lang="en">
  <head>
        @include('layout.head') 
        
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        @include('layout.left-sidebar')
        @include('layout.topnav')
        <div class="right_col" role="main">
                <div class="row">
                      <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                          <div class="x_title">
                            <h2>Patient details</h2>
                            <!-- <ul class="nav navbar-right panel_toolbox">
                              <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                              </li>
                              <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                <ul class="dropdown-menu" role="menu">
                                  <li><a href="#">Settings 1</a>
                                  </li>
                                  <li><a href="#">Settings 2</a>
                                  </li>
                                </ul>
                              </li>
                              <li><a class="close-link"><i class="fa fa-close"></i></a>
                              </li>
                            </ul> -->
                            <div class="clearfix"></div>
                          </div>
                          <div class="x_content" style="display: block;">
                            <br>
                            <form id="demo-form2" data-parsley-validate="" class="form-horizontal form-label-left" novalidate="">
                              <div class="x_title">
                                <h4>Basic Information</h4>
                                <div class="clearfix"></div>
                              </div>
                                
                              <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">First Name <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                  <input type="text" id="first-name" required="required" class="form-control col-md-7 col-xs-12">
                                </div>
                              </div>
                              <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="age">Age <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                  <input type="text" id="age" required="required" class="form-control col-md-7 col-xs-12" name="data[age]">
                                </div>
                              </div>
                              <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="address">Address <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                  <input type="text" id="address" name="address" required="required" class="form-control col-md-7 col-xs-12">
                                </div>
                              </div>
                              <div class="form-group">
                                <label for="phone" class="control-label col-md-3 col-sm-3 col-xs-12">Contact</label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                  <input id="phone" class="form-control col-md-7 col-xs-12" type="text" name="phone">
                                </div>
                              </div>
                              <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Gender</label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                  <div id="gender" class="btn-group" data-toggle="buttons">
                                    <label class="btn btn-default" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
                                      <input type="radio" name="gender" value="male" data-parsley-multiple="gender"> &nbsp; Male &nbsp;
                                    </label>
                                    <label class="btn btn-primary" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
                                      <input type="radio" name="gender" value="female" data-parsley-multiple="gender"> Female
                                    </label>
                                  </div>
                                </div>
                              </div>
                              <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Date Of Birth <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                  <input id="birthday" class="date-picker form-control col-md-7 col-xs-12" required="required" type="text">
                                </div>
                              </div>
                              <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="ref-by">Ref.By <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                  <input type="text" id="refby" required="required" class="form-control col-md-7 col-xs-12" name="data[refby]">
                                </div>
                              </div>

                              <div class="x_title">
                                <h4>Deceses Information</h4>
                                <ul class="nav navbar-right panel_toolbox">
                                   <li><button type="button" class="btn btn-primary" id="desceseinfo">Add</button></li>
                                </ul>   
                                <div class="clearfix"></div>
                              </div>
                              <div id="descesefield">
                              <div class="form-group">
                                <label for="phone" class="control-label col-md-3 col-sm-3 col-xs-12">Descese name</label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                  <input id="descese" class="form-control col-md-7 col-xs-12" type="text" name="descese">
                                </div>
                              </div>
                              </div>

                             <div class="x_title">
                                <h4>Previous Treatment Information</h4>
                                <ul class="nav navbar-right panel_toolbox">
                                   <li><button type="button" class="btn btn-primary" id="addprevinfo">Add</button></li>
                                </ul>   
                                <div class="clearfix"></div>
                              </div>
                              
                            <div id="prevtreat">
                                <div class="form-group">
                                    <label for="phone" class="control-label col-md-3 col-sm-3 col-xs-12">Doctor Name</label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                      <input id="phone" class="form-control col-md-7 col-xs-12" type="text" name="phone">
                                 </div>
                                  </div>
                                  <div class="form-group">
                                    <label for="phone" class="control-label col-md-3 col-sm-3 col-xs-12">Doctor Address</label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                      <input id="phone" class="form-control col-md-7 col-xs-12" type="text" name="phone">
                                    </div>
                                  </div>
                                  <div class="form-group">
                                    <label for="phone" class="control-label col-md-3 col-sm-3 col-xs-12">Doctor Contact no.</label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                      <input id="phone" class="form-control col-md-7 col-xs-12" type="text" name="phone">
                                    </div>
                                  </div>
                                  <div class="form-group">
                                    <label for="phone" class="control-label col-md-3 col-sm-3 col-xs-12">Final Report</label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                      <input id="phone" class="form-control col-md-7 col-xs-12" type="text" name="phone">
                                    </div>
                                  </div>
                              </div>

                            <div class="x_title">
                                <h4>Petient Tests Information</h4>
                                <ul class="nav navbar-right panel_toolbox">
                                   <li><button type="button" class="btn btn-primary" id="addtest">Add</button></li>
                                </ul>
                                <div class="clearfix"></div>
                            </div>
                            <div id="testinfo">
                                <div class="form-group">
                                    <label for="phone" class="control-label col-md-3 col-sm-3 col-xs-12">Test name</label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                      <input id="phone" class="form-control col-md-7 col-xs-12" type="text" name="phone">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="phone" class="control-label col-md-3 col-sm-3 col-xs-12">Test containt</label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                      <input id="phone" class="form-control col-md-7 col-xs-12" type="text" name="phone">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="phone" class="control-label col-md-3 col-sm-3 col-xs-12">Test Report</label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                      <input id="phone" class="form-control col-md-7 col-xs-12" type="text" name="phone">
                                    </div>
                                </div>
                              </div>

                              <div class="x_title">
                                <h4>Petient Before and After Videos</h4>
                                <div class="clearfix"></div>
                            </div>
                              <div class="form-group">
                                <label for="phone" class="control-label col-md-3 col-sm-3 col-xs-12">Before OPD Videos</label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                  <input id="phone" class="form-control col-md-7 col-xs-12" type="text" name="phone">
                                </div>
                              </div>
                              <div class="form-group">
                                <label for="phone" class="control-label col-md-3 col-sm-3 col-xs-12">After OPD Videos</label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                  <input id="phone" class="form-control col-md-7 col-xs-12" type="text" name="phone">
                                </div>
                              </div>
                              
                              <div class="ln_solid"></div>
                              <div class="form-group">
                                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                  <button class="btn btn-primary" type="button">Cancel</button>
                                  <button class="btn btn-primary" type="reset">Reset</button>
                                  <button type="submit" class="btn btn-success">Submit</button>
                                </div>
                              </div>

                            </form>
                          </div>
                        </div>
                      </div>
                    </div>
                    </div>
        @include('layout.footer')
      </div>
    </div>
@include('layout.scripts')
    <script>
        $(document).ready(function(){
            $("#addprevinfo").click(function(){
                    $("#prevtreat").append("<div class='form-group'><label for='phone' class='control-label col-md-3 col-sm-3 col-xs-12'>Doctor Name</label><div class='col-md-6 col-sm-6 col-xs-12'><input id='phone' class='form-control col-md-7 col-xs-12' type='text' name='phone'></div></div>"+
                                  "<div class='form-group'><label for='phone' class='control-label col-md-3 col-sm-3 col-xs-12'>Doctor Address</label><div class='col-md-6 col-sm-6 col-xs-12'><input id='phone' class='form-control col-md-7 col-xs-12' type='text' name='phone'></div></div>"+
                                  "<div class='form-group'><label for='phone' class='control-label col-md-3 col-sm-3 col-xs-12'>Doctor Contact no.</label><div class='col-md-6 col-sm-6 col-xs-12'><input id='phone' class='form-control col-md-7 col-xs-12' type='text' name='phone'></div></div>"+
                                  "<div class='form-group'><label for='phone' class='control-label col-md-3 col-sm-3 col-xs-12'>Final Report</label><div class='col-md-6 col-sm-6 col-xs-12'><input id='phone' class='form-control col-md-7 col-xs-12' type='text' name='phone'></div></div>");
                                  
            });

            $("#desceseinfo").click(function(){
                  $("#descesefield").append("<div class='form-group'><label for='phone' class='control-label col-md-3 col-sm-3 col-xs-12'>Descese name</label><div class='col-md-6 col-sm-6 col-xs-12'><input id='descese' class='form-control col-md-7 col-xs-12' type='text' name='descese'></div></div>");
                                  
            });

            $("#addtest").click(function(){
                  $("#testinfo").append("<div class='form-group'><label for='phone' class='control-label col-md-3 col-sm-3 col-xs-12'>Test name</label><div class='col-md-6 col-sm-6 col-xs-12'><input id='phone' class='form-control col-md-7 col-xs-12' type='text' name='phone'></div></div>"+
                                "<div class='form-group'><label for='phone' class='control-label col-md-3 col-sm-3 col-xs-12'>Test containt</label><div class='col-md-6 col-sm-6 col-xs-12'><input id='phone' class='form-control col-md-7 col-xs-12' type='text' name='phone'></div></div>"+
                                "<div class='form-group'><label for='phone' class='control-label col-md-3 col-sm-3 col-xs-12'>Test Report</label><div class='col-md-6 col-sm-6 col-xs-12'><input id='phone' class='form-control col-md-7 col-xs-12' type='text' name='phone'></div></div>");
                                  
            });


        });
        </script>
    
  </body>
</html>
