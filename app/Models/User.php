<?php

namespace App\Models;

//use Illuminate\Foundation\Auth\User as Authenticatable;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    protected $table='users';

    function __construct($property=[]){
        if(!empty($property)){
            foreach($property as $key=>$value){
                $this->{$key} = $value;
            }
        }
    }

    function updateFields($property=[]){
        if(!empty($property)){
            foreach($property as $key=>$value){
                $this->{$key} = $value;
            }
        }
    }
protected $hidden=['username','password'];
}
