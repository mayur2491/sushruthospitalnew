<?php

namespace App\Models;

//use Illuminate\Foundation\Auth\User as Authenticatable;

use Illuminate\Database\Eloquent\Model;

class NurseReview extends Model
{
    protected $table='nurse_reviews';

    function __construct($property=[]){
        if(!empty($property)){
            foreach($property as $key=>$value){
                $this->{$key} = $value;
            }
        }
    }

    function updateFields($property=[]){
        if(!empty($property)){
            foreach($property as $key=>$value){
                $this->{$key} = $value;
            }
        }
    }

    function nurse(){
        return $this->belongsTo('App\Models\Nurse');
    }
protected $hidden=['username','password'];
}
