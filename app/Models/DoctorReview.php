<?php

namespace App\Models;

//use Illuminate\Foundation\Auth\User as Authenticatable;

use Illuminate\Database\Eloquent\Model;

class DoctorReview extends Model
{
    protected $table='doctor_reviews';

    function __construct($property=[]){
        if(!empty($property)){
            foreach($property as $key=>$value){
                $this->{$key} = $value;
            }
        }
    }

    function updateFields($property=[]){
        if(!empty($property)){
            foreach($property as $key=>$value){
                $this->{$key} = $value;
            }
        }
    }

    function doctor(){
        return $this->belongsTo('App\Models\Doctor');
    }
protected $hidden=['username','password'];
}
