<?php
namespace App\Http\Controllers;
use App\Models\Patient;
use App\Models\Descese;
use App\Models\Nurse;
use App\Models\NurseReview;
use App\Models\DoctorReview;
use App\Models\Doctor;
use App\Models\Test;
use App\Models\PreviousTreatment;
use App\Models\IpdPetients;
use App\Models\Treatments;
use Illuminate\Http\Request;
//use Illuminate\Support\Facades\Request;


class IpdPatientController extends Controller
{


	public function display(){


		$ipdpatients=IpdPetients::paginate('10');

		
		foreach ($ipdpatients as $ipdpatient) {
			//print_r($ipdpatient->id);
			
			$patient = Patient::find($ipdpatient->petient_id);
			
			$ipdpatient->name=($patient->name) ? $patient->name : '' ;
			$ipdpatient->age=($patient->age) ? $patient->age : '' ;
			$ipdpatient->contact=($patient->contact) ? $patient->contact : '' ;
			$ipdpatient->address=($patient->address) ? $patient->address : '' ;
			$ipdpatient->gender=($patient->gender) ? $patient->gender : '' ;
			$ipdpatient->date_of_birth=($patient->date_of_birth) ? $patient->date_of_birth : '' ;
			$ipdpatient->refby=($patient->refby) ? $patient->refby : '' ;

		}
		
        return view('ipdpetient.display')->with('ipdpatients', $ipdpatients);
    }

    public function edit(Request $request){

        $regno = $request->get('regno');
        
        $flag=1;
        
        if(isset($regno)){

            $IPDpatient = IpdPetients::find($regno);
            $treatment = Treatments::where('regno',$regno)->first();
            
            $nurses= Nurse::All();
            $doctors= Doctor::All();
            $nursereviews=NurseReview::where('ipdpetient_id',$IPDpatient->id)->get();
            $doctorreviews=DoctorReview::where('ipdpetient_id',$IPDpatient->id)->get();



            $patient = Patient::find($IPDpatient->petient_id);

            if(isset($IPDpatient)){
                $request->session()->put('regno', $regno);
                return view('ipdpetient.edit')->with('ipdpatient', $IPDpatient)->with('patient',$patient)->with('nurses',$nurses)->with('nursereviews',$nursereviews)->with('doctorreviews',$doctorreviews)->with('doctors',$doctors)->with('treatment',$treatment);
            }
            else
                return json_encode(array('message' => 'invalid'));
        }
        else
            return json_encode(array('message' => 'invalid'));
    }

 public function update(Request $request)
    {

        $regno = $request->session()->get('regno');
        


        if(isset($regno)) {

            $updateuser=IpdPetients::where(array('regno'=> $regno))->first();
            $treatment = Treatments::where(array('regno'=> $regno))->first();

            if (isset($updateuser)) {

                $passdata=$request->get('data');
                
                $treatment_info=$request->get('treatment');
                
                
               foreach($passdata as $key => $value)
                    $data[$key] = $value;

               $updateuser->updateFields($data);
               $updateuser->save();


                
                foreach($treatment_info as $key => $value)
                    $treatmentdata[$key] = $value;
                $treatmentdata['regno']=$regno;

                if(count($treatment)>0)
                {
                    $treatment->updateFields($treatmentdata);
                    $treatment->save();
                }else{

                    $treatments=new Treatments($treatmentdata);
                    $treatments->save();    
                }                
                

                return json_encode(array('message' => 'done'));
            }
            else
                return json_encode(array('message' => 'invalid'));
        }
        else
            return json_encode(array('message' => 'invalid'));

    }


    public function getMlcCount(Request $request)
    {
            $count_mlc=IpdPetients::where(array('is_mlc'=> 1))->get()->count();
            
            return json_encode(array('count' => $count_mlc));
    }

}