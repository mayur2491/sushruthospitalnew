<?php

namespace App\Http\Controllers\Auth;


use App\Models\Customer;
use App\User;

use App\Http\Controllers\Controller;
//use App\Http\Requests\Request;
//use Symfony\Component\HttpFoundation\Request;

//use Illuminate\Foundation\Auth\ThrottlesLogins;
//use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Models\CartItem;
use App\Models\Cart;
class AuthController extends Controller
{
    public function checkLogin(Request $request)
    {
        $username = $request->get('username');
        $password = $request->get('password');

        $user = User::where(array('username' => $username, 'password' => $password,'status'=>'active'))->first();

        if (isset($user)) {
            Session::put('name',$user->name);
            Session::put('id',$user->id);
			Session::put('usertype',$user->usertype);
            Session::put('profile_img',$user->profile_img);
				

            return json_encode(array('message' => 'correct'));
        } else
            return json_encode(array('message' => 'wrong'));

    }
    public function logout(){
        Session::flush();
        return redirect('/login');
    }

    /*public function checkWebLogin(Request $request)
    {
        $username = $request->get('username');
        $password = $request->get('password');
        $customer = Customer::where(array('username' => $username, 'password' => $password,'status'=>'active'))->first();

        if (isset($customer)) {

             Session::put('customer_name',$customer->name);
             Session::put('customer_id',$customer->id);
			//Session::put('usertype',$user->usertype);
			
			$cart=Cart::where(array('customer_id'=>$customer->id))->first();
			$Itemcount=CartItem::where(array('cart_id'=>$cart->id))->get();
			Session::put('count',count($Itemcount));
           
			return json_encode(array('message' => 'correct'));
        } else
            return json_encode(array('message' => 'wrong'));

    }
    public function weblogout(){
        Session::forget('customer_name');
        Session::forget('customer_id');
		Session::forget('count');
        return redirect('/');
    }*/
}
