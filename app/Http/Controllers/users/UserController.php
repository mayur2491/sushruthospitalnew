<?php
namespace App\Http\Controllers\users;
use App\Http\Controllers\Controller;
use App\Models\Doctor;
use App\Models\Descese;
use App\Models\User;
use App\Models\Test;
use App\Models\PreviousTreatment;
use App\Models\IpdPetients;
use Illuminate\Http\Request;
//use Illuminate\Support\Facades\Request;


class UserController extends Controller
{


	public function display(){


		$users=User::paginate('10');

        return view('user.display')->with('users', $users);
    }

    public function save(Request $request){

        $passdata=$request->get('data');

        foreach($passdata as $key => $value)
            $data[$key] = $value;
                
        $data['usertype']="subuser";
        

        $target_dir = "public/images/uploads/";
        $target_file = $target_dir . basename($_FILES["profile_img"]["name"]);
        
        $targetpath=$this->fileuploads($target_file);

        $data['profile_img']=$targetpath;
        $data['status']="active";

        $users=new User($data);
        $result=$users->save();
        if($result)
        {
            return json_encode(array('message' => 'done'));
        }
        else
        {
            return json_encode(array('message' => 'wrong'));
        }
    }

    public function fileuploads($target_file){

        $uploadOk = 1;
        $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
        // Check if image file is a actual image or fake image
        if(isset($_POST["submit"])) {
            $check = getimagesize($_FILES["profile_img"]["tmp_name"]);
            if($check !== false) {
                return $targetfile='';
                $uploadOk = 1;
            } else {
                return $targetfile='';
                $uploadOk = 0;
            }
        }

        if ($uploadOk == 0) {
            return $targetfile='';
        // if everything is ok, try to upload file
        } else {
            if (move_uploaded_file($_FILES["profile_img"]["tmp_name"], $target_file)) {
                return $targetpath=$target_file;
            } else {
                return $targetfile='';
            }
        }

    }

    
    public function edit(Request $request){

        $id = $request->get('id');
        $flag=1;

        if(isset($id)){

            $user = User::find($id);
            

            if(isset($user)){
                $request->session()->put('id', $id);
                return view('User.edit')->with('user', $user);
            }
            else
                return json_encode(array('message' => 'invalid'));
        }
        else
            return json_encode(array('message' => 'invalid'));
    }

 public function update(Request $request)
    {

        $id = $request->session()->get('id');

        if(isset($id)) {

            $user=User::where(array('id'=>$id ))->first();

            if (isset($user)) {

                $passdata=$request->get('data');
                foreach($passdata as $key => $value)
                    $data[$key] = $value;
                    

                    $data['usertype']="subuser";

                    $target_dir = "public/images/uploads/";
                    $target_file = $target_dir . basename($_FILES["profile_img"]["name"]);
                    
                    $targetpath=$this->fileuploads($target_file);

                    $data['profile_img']=$targetpath;
                    $data['status']="active";

                $user->updateFields($data);

                $user->save();

                return json_encode(array('message' => 'done'));
            }
            else
                return json_encode(array('message' => 'invalid'));
        }
        else
            return json_encode(array('message' => 'invalid'));

    }

}