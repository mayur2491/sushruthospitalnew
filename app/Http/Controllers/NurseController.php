<?php
namespace App\Http\Controllers;
use App\Models\Nurse;
use App\Models\Descese;
use App\Models\Test;
use App\Models\PreviousTreatment;
use App\Models\IpdPetients;
use Illuminate\Http\Request;
use Exception;
//use Illuminate\Support\Facades\Request;


class NurseController extends Controller
{


	public function display(){


		$nurses=Nurse::orderBy('id', 'DESC')->get();

        return view('Nurse.display')->with('nurses', $nurses);
    }

    public function save(Request $request){

    try{
            $passdata=$request->get('data');

            foreach($passdata as $key => $value)
                $data[$key] = $value;
                    
            $nurse=new Nurse($data);
            $result=$nurse->save();
            $data['id']=$nurse->id;
            if($result)
            {
                return json_encode(array('status'=>'success','message' => 'Data Inserting SuccessFully','data'=>$data));
            }
            else
            {
                return json_encode(array('status'=>'failed','message' => "Something went wrong please try again...."));
            }

        } catch (Exception $e) {
                return json_encode(array('status'=>'failed','message' => "Something went wrong please try again...."));
        }    
    }

    public function edit(Request $request){

        $id = $request->get('id');
        $flag=1;

        if(isset($id)){

            $nurse = Nurse::find($id);
            if(isset($nurse)){
                $request->session()->put('id', $id);
                return view('Nurse.edit')->with('nurse', $nurse);
            }
            else
                return json_encode(array('message' => 'invalid'));
        }
        else
            return json_encode(array('message' => 'invalid'));
    }

 public function update(Request $request)
    {

        try{
                $id = $request->session()->get('id');

                if(isset($id)) {

                    $nurse=Nurse::where(array('id'=>$id ))->first();

                    if (isset($nurse)) {

                        $passdata=$request->get('data');
                        foreach($passdata as $key => $value)
                            $data[$key] = $value;
                           
                        $nurse->updateFields($data);

                        $result=$nurse->save();

                        if($result)
                        {
                            return json_encode(array('status'=>'success','message' => 'Data Updated SuccessFully'));
                        }
                        else
                        {
                            return json_encode(array('status'=>'failed','message' => "Something went wrong please try again...."));
                        }

                    }else{
                        return json_encode(array('status'=>'failed','message' => "Something went wrong please try again...."));
                    }
                }
                else{
                    return json_encode(array('status'=>'failed','message' => "Something went wrong please try again...."));
                }


        } catch (Exception $e) {
            return json_encode(array('status'=>'failed','message' => "Something went wrong please try again...."));
        }

    }

}