<?php
namespace App\Http\Controllers;
use App\Models\Doctor;
use App\Models\Descese;
use App\Models\Test;
use App\Models\PreviousTreatment;
use App\Models\IpdPetients;
use Illuminate\Http\Request;
use Exception;
//use Illuminate\Support\Facades\Request;


class DoctorController extends Controller
{


	public function display(){


		$doctors=Doctor::orderBy('id', 'DESC')->get();
        //->paginate('10');

        return view('Doctor.display')->with('doctors', $doctors);
    }

    public function save(Request $request){

        try{
                $passdata=$request->get('data');

                foreach($passdata as $key => $value)
                    $data[$key] = $value;
                
                $doctor=new Doctor($data);

                $result=$doctor->save();
                $data['id']=$doctor->id;
                if($result)
                {
                    return json_encode(array('status'=>'success','message' => 'Data Inserting SuccessFully','data'=>$data));
                }
                else
                {
                    return json_encode(array('status'=>'failed','message' => "Something went wrong please try again...."));
                }
            } catch (Exception $e) {
                return json_encode(array('status'=>'failed','message' => "Something went wrong please try again...."));
                

            }
    }

    public function edit(Request $request){

            try{
                $id = $request->get('id');
                $flag=1;

                if(isset($id)){

                    $doctor = Doctor::find($id);
                   
                    if(isset($doctor)){
                        $request->session()->put('id', $id);
                        return view('Doctor.edit')->with('doctor', $doctor);
                    }
                    else
                        return json_encode(array('message' => 'invalid'));
                }
                else
                    return json_encode(array('message' => 'invalid'));

            } catch (Exception $e) {
                return json_encode(array('status'=>'failed','message' => "Something went wrong please try again...."));
            }
    }

 public function update(Request $request)
    {

    try{
            $id = $request->session()->get('id');

            if(isset($id)) {

                $doctor=Doctor::where(array('id'=>$id ))->first();

                if (isset($doctor)) {

                    $passdata=$request->get('data');
                    foreach($passdata as $key => $value)
                        $data[$key] = $value;

                    $doctor->updateFields($data);

                   $result=$doctor->save();


                    if($result)
                    {
                        return json_encode(array('status'=>'success','message' => 'Data Updated SuccessFully'));
                    }
                    else
                    {
                        return json_encode(array('status'=>'failed','message' => "Something went wrong please try again...."));
                    }

                }
                else{
                    return json_encode(array('status'=>'failed','message' => "Something went wrong please try again...."));
                }
            }
            else{
                return json_encode(array('status'=>'failed','message' => "Something went wrong please try again...."));
            }
        } catch (Exception $e) {
            return json_encode(array('status'=>'failed','message' => "Something went wrong please try again...."));
        }

    }

}