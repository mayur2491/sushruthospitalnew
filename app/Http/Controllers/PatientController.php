<?php
/**
 * Created by PhpStorm.
 * User: lenovo
 * Date: 04/06/2016
 * Time: 10:13
 */

namespace App\Http\Controllers;
use App\Models\Patient;
use App\Models\Descese;
use App\Models\Test;
use App\Models\PreviousTreatment;
use App\Models\IpdPetients;
use Illuminate\Http\Request;
//use Illuminate\Support\Facades\Request;


class PatientController extends Controller
{
    
	public function create(){
    
		return view('manage_patient.create');
    
	}

    public function save(Request $request){

        //$user=new User();
        $passdata=$request->get('data');

        foreach($passdata as $key => $value)
            $data[$key] = $value;
        $data['advice']='opd';
        $bdate=explode('/', $data['date_of_birth']);
        $data['date_of_birth']=$bdate[2]."-".$bdate[0]."-".$bdate[1];
        
        $users=new Patient($data);
        $result=$users->save();
        if($result)
        {
            return json_encode(array('message' => 'done'));
        }
        else
        {
            return json_encode(array('message' => 'wrong'));
        }
    }


    public function saveipd(Request $request){

        $passdata=$request->get('data');

        foreach($passdata as $key => $value)
            $data[$key] = $value;
        
        $ipdpetient=IpdPetients::max('id')+1;
        $monyear=explode(' ',date('M Y'));

        $data['regno']=$ipdpetient.'-'.$monyear[0].'-'.$monyear[1];
        
        $users=new IpdPetients($data);
        $result=$users->save();
        if($result)
        {
            return json_encode(array('message' => 'done'));
        }
        else
        {
            return json_encode(array('message' => 'wrong'));
        }
    }

    public function display(){
       
       $patients=Patient::paginate('10');
       return view('manage_patient.display')->with('patients', $patients);
    }
    public function ajaxGetPatient(Request $request){
       
       $patients=Patient::paginate('10');
       
       return view('manage_patient.display')->with('patients', $patients);
    }


    public function edit(Request $request){

        $id = $request->get('petient_id');
        $flag=1;

        if(isset($id)){

            $patient = Patient::find($id);
            $dob=explode('-',$patient->date_of_birth);
            $patient->date_of_birth=$dob[1].'/'.$dob[2].'/'.$dob[0];
            $desceses = Descese::where('petient_id',$id)->pluck('descese_name')->toArray();
                        
            $descese_name=implode(",", $desceses);
            $patient->descese_name=$descese_name;
            $tests = Test::where('petient_id',$id)->get();
            $ipdpetients = IpdPetients::where('petient_id',$id)->get();
            
            if(!$ipdpetients->isEmpty()){
                $flag=1;
            }else{
                $flag=0;
            }
            if(isset($patient)){
                $request->session()->put('petient_id', $id);
                return view('manage_patient.edit')->with('patient', $patient)->with('tests', $tests)->with('flag', $flag);
            }
            else
                return json_encode(array('message' => 'invalid'));
        }
        else
            return json_encode(array('message' => 'invalid'));
    }

    public function update(Request $request)
    {

        $id = $request->session()->get('petient_id');

        if(isset($id)) {

            $updateuser=Patient::where(array('id'=>$id))->first();


            if (isset($updateuser)) {

                $passdata=$request->get('data');


                foreach($passdata as $key => $value)
                    if($key=='descese_name')
                    {
                        $descese_data[$key]=$value;
                    }else{
                        $data[$key] = $value;    
                    }

                    
                $bdate=explode('/', $data['date_of_birth']);
                $data['date_of_birth']=$bdate[2]."-".$bdate[0]."-".$bdate[1];
                $descese=explode(',', $descese_data['descese_name']);
                
                $delete_descese = Descese::where(array('petient_id'=>$id))->delete();

                foreach ($descese as $key => $value) {

                    $descese_current=Descese::where(array('petient_id'=>$id,'descese_name'=>$value))->first();
                    $desc_data['descese_name']=$value;
                    $desc_data['petient_id']=$id;
                    

                    if(!empty($descese_current)){
                        $descese_current->updateFields($desc_data);
                        $descese_current->save();
                    }else{
                        $descese=new Descese($desc_data);
                        $result=$descese->save();
                    }
                }
                                
                $updateuser->updateFields($data);

                $updateuser->save();

                return json_encode(array('message' => 'done'));
            }
            else
                return json_encode(array('message' => 'invalid'));
        }
        else
            return json_encode(array('message' => 'invalid'));

    }

    public function find(Request $request){

        $searchfield = $request->get('searchfield');
        $searchdata = $request->get('searchdata');
        

        $users = User::where(array([$searchfield,$searchdata]))->get();
        return view('user.records')->with('users', $users);
    }

    public function remove(Request $request)
    {

        $id = $request->get('data');
        $flag=0;
        //print_r($id);
        if(isset($id))
        {
            //echo sizeof($id);
            for($i=0;$i<sizeof($id);$i++)
            {
                //echo$id[$i];
                $user = User::find($id[$i]);

                if (isset($user))
                {

                    $data['status'] = 'removed';
                    $user ->updateFields($data);

                    $user ->save();

                    $flag=1;
                    //return json_encode(array('message' => 'successfully'));

                }
                else
                {
                    $flag=0;
                    return json_encode(array('message' => 'invalid'));
                    break;
                }

            }
             if($flag==1)
             {
                 return json_encode(array('message' => 'done'));
             }
            else
            {
                return json_encode(array('message' => 'invalid'));
            }

        }
        else
        {

            return json_encode(array('message' => 'invalidid'));
        }

    }


    public function save_test(Request $request){

        //$user=new User();
        $passdata=$request->get('data');
        
        foreach($passdata as $key => $value)
            $data[$key] = $value;
        $data['petient_id']=$request->session()->get('petient_id');
        $tests=new Test($data);

        $result=$tests->save();
        if($result)
        {
            return json_encode(array('message' => 'done','data'=>$tests));
        }
        else
        {
            return json_encode(array('message' => 'wrong' ,'data'=>''));
        }
    }

    public function save_descese(Request $request){

        //$user=new User();
        $passdata=$request->get('data');
        
        foreach($passdata as $key => $value)
            $data[$key] = $value;

        $descese=new Descese($data);

        $result=$descese->save();
        if($result)
        {
            return json_encode(array('message' => 'done','data'=>$descese));
        }
        else
        {
            return json_encode(array('message' => 'wrong' ,'data'=>''));
        }
    }

    public function save_previous_treatment(Request $request){

        //$user=new User();
        $passdata=$request->get('data');
        
        foreach($passdata as $key => $value)
            $data[$key] = $value;
        $previoustreatmentsdata=new PreviousTreatment($data);

        $result=$previoustreatmentsdata->save();
        if($result)
        {
            return json_encode(array('message' => 'done','data'=>$previoustreatmentsdata));
        }
        else
        {
            return json_encode(array('message' => 'wrong' ,'data'=>''));
        }
    }


}
