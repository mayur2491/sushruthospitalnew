<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class CustomAuthenticate
{
    public function handle(Request $request, Closure $next)
    {
        $logged = session()->has('id');//session id

        if (!$logged) {
            if ($request->ajax()) {
                return response('Unauthorized.', 401);
            } else {
                return redirect()->guest('/login');
            }
        }

        return $next($request);
        /************** checking if session is set or not ******************/


    }
}