<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});*/

Route::get('/dashboard', function () {
    return view('dashboard');
});


/*web route*/

Route::get('/', function () {
    return view('home');
});

Route::get('/manage_patient', function () {
    return view('manage_patient.manage_patient');
});

Route::get('/login', function () {
    return view('login.login');
});

Route::post('/autentication', 'Auth\AuthController@checklogin');

Route::get('/logout', 'Auth\AuthController@logout');

Route::group(['middleware' => ['custom_authenticate']], function () {

Route::get('/create', 'PatientController@create');
Route::post('/save', 'PatientController@save');
Route::post('/update', 'PatientController@update');
Route::get('/manage', 'PatientController@display');
Route::get('/records', 'PatientController@records');
Route::get('/edit', 'PatientController@edit');
Route::post('/save_test', 'PatientController@save_test');
Route::post('/save_descese', 'PatientController@save_descese');
Route::post('/saveipd', 'PatientController@saveipd');
Route::get('/manageipdpetient', 'IpdPatientController@display');
Route::post('/save-previous-treatment', 'PatientController@save_previous_treatment');

Route::get('/ajaxgetpatient', 'PatientController@ajaxGetPatient');

Route::get('/ipdpetientedit', 'IpdPatientController@edit');
Route::post('/ipdpetientupdate', 'IpdPatientController@update');
Route::get('/getmlccount', 'IpdPatientController@getMlcCount');


Route::get('/doctors', 'DoctorController@display');
Route::post('/save-doctor', 'DoctorController@save');
Route::get('/edit-doctor', 'DoctorController@edit');
Route::post('/update-doctor', 'DoctorController@update');

Route::get('/nurse', 'NurseController@display');
Route::post('/save-nurse', 'NurseController@save');
Route::get('/edit-nurse', 'NurseController@edit');
Route::post('/update-nurse', 'NurseController@update');


Route::post('/save-nursereview', 'NurseReviewController@save');
Route::post('/save-doctorreview', 'DoctorReviewController@save');


Route::get('/users', 'UserController@display');
Route::post('/save-user', 'UserController@save');
Route::get('/edit-user', 'UserController@edit');
Route::post('/update-user', 'UserController@update');


});


/*Route::group(['middleware' => ['custom_authenticate'] , 'prefix'=>'users' , 'namespace'=>'users'], function () {

	Route::get('/users', 'UserController@display');
	Route::post('/save-user', 'UserController@save');
	Route::get('/edit-user', 'UserController@edit');
	Route::post('/update-user', 'UserController@update');


});*/