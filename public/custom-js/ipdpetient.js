$(function() {


   
    update();
    update_nursereview();
    save_doctorreview();


      $('#summernote').summernote({
        placeholder: 'Please Enter Advice',
        tabsize: 2,
        height: 100
      });
      
      $('#summernote1').summernote({
        
        placeholder: 'Please Enter Prescriptions',
        tabsize: 2,
        height: 100
      });

      $(".form_datetime").datetimepicker({
        format: "yyyy-mm-dd hh:ii",
        showMeridian: true,
        autoclose: true,
        todayBtn: true
    });



});

function update()
{
    $("#editipdform").on('submit', function (e) {
        e.preventDefault();

        $.ajax({
            url: root + "ipdpetientupdate",
            type: "POST",
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            success: function (result) {
                
               // window.location.reload();

            }
        });

    });
}

function update_nursereview()
{
    $("#nursereview-form").on('submit', function (e) {
        e.preventDefault();

        $.ajax({
            url: root + "save-nursereview",
            type: "POST",
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            success: function (result) {
                
                window.location.reload();

            }
        });

    });
}


function save_doctorreview()
{
    $("#doctorsreview-form").on('submit', function (e) {
        e.preventDefault();

        $.ajax({
            url: root + "save-doctorreview",
            type: "POST",
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            success: function (result) {
                
                window.location.reload();

            }
        });

    });
}


function getMlcCount()

{
    var category=$("#category").val();
    
    
    if(category ==1){

        var url=root+"getmlccount";
            $.ajax({
                url: url,
                type:"get",
                dataType:"json",
                success: function (result) {
                    $("#category_div").addClass("col-md-8").removeClass("col-md-12");
                    $("#mlccount").show();
                    $("#mlccount").html("MLC Number:-"+result.count);

                }
            });
        
    }else{
         $("#mlccount").hide();
         $("#category_div").addClass("col-md-12").removeClass("col-md-8");

    }


}