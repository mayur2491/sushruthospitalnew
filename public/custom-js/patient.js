$(function() {


    save();
    create();
    
    search();
    
    update();
    deleterecord();
    save_test();
    save_descese();
    previous_treatment();
    save_ipdform();
     

});

function create()
{
    $("#adduser").click(function () {
        var url=root+"user/create";
        $.ajax({
            url: url,datatype:"json",success: function (result) {

                $(".databody").html(result);

            }
        });
    });
}

function update()
{
    $("#editform").on('submit', function (e) {
        e.preventDefault();

       /* var values = $("input[name='descese_name[]']")
              .map(function(){return $(this).val();}).get();
              alert(values);*/
        //var values = $("input[name='descese_name']").val();
        //alert(values);
        $.ajax({
            url: root + "update",
            type: "POST",
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            success: function (result) {
                
                window.location.reload();

            }
        });

    });
}
function search()
{
    $("#search").click(function () {

        var searchfield=$("#selectedfield").val();
        var searchdata=$("#searchdata").val();

        var url=root+"user/search?searchfield="+searchfield+"&searchdata="+searchdata;
        $.ajax({
            url: url,datatype:"json",success: function (result) {
                //alert(result);
                $(".panel-body").html(result);

            }
        });
    });

}

function save()
{
   $("#patient-form").on('submit', function (e) {
        e.preventDefault();
        
        $.ajax({
            url: root + "save",
            type: "POST",
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
      dataType:"json",
            success: function (result) {
      if(result.message=="done")
      { 
        window.location.replace(root+'manage');
      }
      else
        $("#message").html("User addition Failed").css('color','red');

            }
        });

    });
}


function save_ipdform()
{
    $("#ipd-form").on('submit', function (e) {
        e.preventDefault();

        $.ajax({
            url: root + "saveipd",
            type: "POST",
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            dataType:"json",
            success: function (result) {
                           
                    window.location.reload();
            }
        });

    });
}

function deleterecord()
{
    $("#delete").click(function () {

        var data = Array();
        $('input[name="selected"]:checked').each(function() {
            data.push($(this).val());
        });

        var url=root+"user/remove";
		if(confirm("Are you sure do you want to remove ? ")) {
			$.ajax({
				url: url,
				data:{data:data},
				type:"get",
				dataType:"json",
				success: function (result) {
					
					window.location.reload();

				}
			});
		}
    });

}

function save_test()
{
    $("#test-form").on('submit', function (e) {
        e.preventDefault();
        
        $.ajax({
            url: root + "save_test",
            type: "POST",
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            dataType:"json",
            success: function (result) {
            if(result.message=="done")
            {  
                
                $("#testmodal").modal('toggle');
                $('#test-form')[0].reset();
                $("#test-tbody").append("<tr><td>"+result.data['test_name']+"</td><td>"+result.data['test_containt']+"</td><td>"+result.data['test_report']+"</td></tr>");


            }
            else
                $("#message").html("User addition Failed").css('color','red');

            }
        });

    });
}

function save_descese()
{
    $("#descese-form").on('submit', function (e) {
        e.preventDefault();
        
        $.ajax({
            url: root + "save_descese",
            type: "POST",
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            dataType:"json",
            success: function (result) {
            if(result.message=="done")
            {  
                $("#descesemodal").modal('toggle');
                $('#descese-form')[0].reset();
                $("#descese-tbody").append("<tr><td>"+result.data['descese_name']+"</td><td>"+result.data['petient_id']+"</td></tr>");


            }
            else
                $("#message").html("User addition Failed").css('color','red');

            }
        });

    });
}

function previous_treatment()
{
    $("#prevtreatment-form").on('submit', function (e) {
        e.preventDefault();
        
        $.ajax({
            url: root + "save-previous-treatment",
            type: "POST",
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            dataType:"json",
            success: function (result) {
            if(result.message=="done")
            {  
                $("#descesemodal").modal('toggle');
                $('#descese-form')[0].reset();
                $("#descese-tbody").append("<tr><td>"+result.data['descese_name']+"</td><td>"+result.data['petient_id']+"</td></tr>");

            }
            else
                $("#message").html("User addition Failed").css('color','red');

            }
        });

    });
}

function change_reg_type()
{


    var regitype=$("#regi_type").val();
    if(regitype =="manual"){
    
        $("#regno").show();
        //$("#regi_type_div").html("<input id='regno' class='form-control col-md-7 col-xs-12' type='text' name='data[regno]'>");
    }else{
        $("#regno").hide();
    }


}

function addField()
{
    
    var descese_field="<div class='col-md-6 col-sm-6 col-xs-12 col-md-offset-3 mar-top-10'><input id='descese_name' class='form-control col-md-7 col-xs-12' type='text' name='descese_name[]'></div>";
$("#descese_field").append(descese_field);

}


function getMlcCount()

{
    var category=$("#category").val();
    
    
    if(category ==1){

        var url=root+"getmlccount";
            $.ajax({
                url: url,
                type:"get",
                dataType:"json",
                success: function (result) {
                    $("#category_div").addClass("col-md-9").removeClass("col-md-12");
                    $("#mlccount").show();
                    $("#mlccount").html("MLC Count:-"+result.count);

                }
            });
        
    }else{
         $("#mlccount").hide();
         $("#category_div").addClass("col-md-12").removeClass("col-md-9");

    }


}