$(function() {

    save();
    update();
 
});

function save()
{
   $("#user-form").on('submit', function (e) {
        e.preventDefault();
        
        $.ajax({
            url: root + "save-user",
            type: "POST",
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            dataType:"json",
            success: function (result) {
      if(result.message=="done")
      { 
        //window.location.replace(root+'manage');
      }
      else
        $("#message").html("User addition Failed").css('color','red');

            }
        });

    });
}


function update()
{
    $("#edituserform").on('submit', function (e) {
        e.preventDefault();

       $.ajax({
            url: root + "update-user",
            type: "POST",
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            success: function (result) {
                
                //window.location.reload();

            }
        });

    });
}


$('#profile_img').change( function(event) {
    
    $("#profile_pic img").fadeIn("fast").attr('src',URL.createObjectURL(event.target.files[0]));
});