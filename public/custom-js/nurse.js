$(function() {

    save();
    update();
 
});

function save()
{
   $("#nurse-form").on('submit', function (e) {
        e.preventDefault();
        
        $.ajax({
            url: root + "save-nurse",
            type: "POST",
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            dataType:"json",
            success: function (result) {
                if(result.status=="success")
                  { 
                        $("#NurseFormModal").modal('toggle');
                        $('#nurse-form')[0].reset();
                        $('.tagsinput .tag').remove();
                        $("#message").html("<div class='alert alert-success'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><strong>Success!</strong>"+result.message+" </div>").css('display','block');
                        $("#nurselist-tbody").prepend("<tr><td><input type='checkbox' onclick='$('input[name*=\'selected\']').prop('checked', this.checked);'></td><td>"+result.data['name']+"</td><td>"+result.data['contact']+"</td><td>"+result.data['education']+"</td><td><a class='btn btn-primary edituser btn-xs'  href='"+root+"edit-nurse?id="+result.data['id']+"'><i class='glyphicon glyphicon-pencil'></i></a></td></tr>");

                  }
                  else{
                      $("#message").html("<div class='alert alert-danger'><strong>Failed!</strong>"+result.message+" </div>").css('display','block');

                    }
            }
        });

    });
}


function update()
{
    $("#editnurseform").on('submit', function (e) {
        e.preventDefault();

       $.ajax({
            url: root + "update-nurse",
            type: "POST",
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            dataType:"json",
            success: function (result) {
             
                if(result.status=="success")
                {
                    $("#message").html("<div class='alert alert-success'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><strong>Success!</strong>"+result.message+" </div>").css('display','block');
                }else{
                    $("#message").html("<div class='alert alert-danger'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><strong>Failed!</strong>"+result.message+" </div>").css('display','block');

                }

            }
        });

    });
}